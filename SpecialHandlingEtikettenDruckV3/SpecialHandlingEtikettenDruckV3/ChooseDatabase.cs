﻿using System;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    
    public partial class frmChooseDatabase : Form
    {
        public  delegate void GetDatabase(Currency dbName);
        public delegate void ImDone();
        public static event GetDatabase SetDatabaseCurrency;
        public static event ImDone ActionAfterImDone;

        public frmChooseDatabase()
        {
            InitializeComponent();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (radBtnChf.Checked) { 
                SetDatabaseCurrency(Currency.Chf); }
            if (radBtnEuro.Checked) { 
                SetDatabaseCurrency(Currency.Eur); }
            ActionAfterImDone();
            this.Close();
        }
    }
}
