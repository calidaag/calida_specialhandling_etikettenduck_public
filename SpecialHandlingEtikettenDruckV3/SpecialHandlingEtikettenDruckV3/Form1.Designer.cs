﻿namespace SpecialHandlingEtikettenDruckV3
{
    partial class frmMainStartup
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainStartup));
            this.lblRecordCount = new System.Windows.Forms.Label();
            this.lblChoosenPrinterInterface = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danielmuellercalidacomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.druckerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbxToolstripeChooseComport = new System.Windows.Forms.ToolStripComboBox();
            this.tCPIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDbPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelAlsQuelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_hitCounter = new System.Windows.Forms.Label();
            this.TboIpAdresse = new System.Windows.Forms.TextBox();
            this.lblIpAddress = new System.Windows.Forms.Label();
            this.lblCurrentDbPath = new System.Windows.Forms.Label();
            this.tabMainNavigation = new System.Windows.Forms.TabControl();
            this.tabPrintPage = new System.Windows.Forms.TabPage();
            this.tboWarning = new System.Windows.Forms.TextBox();
            this.tboCurrency2 = new System.Windows.Forms.TextBox();
            this.tboCurrency1 = new System.Windows.Forms.TextBox();
            this.btnSetUserDefault = new System.Windows.Forms.Button();
            this.lblDeclaration = new System.Windows.Forms.Label();
            this.tboDeclaration = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.tboPrice = new System.Windows.Forms.TextBox();
            this.tboArtDescription = new System.Windows.Forms.TextBox();
            this.lblStoreName = new System.Windows.Forms.Label();
            this.tboColor = new System.Windows.Forms.TextBox();
            this.lblColor = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.tboEan13 = new System.Windows.Forms.TextBox();
            this.tboSize = new System.Windows.Forms.TextBox();
            this.tboColorCode = new System.Windows.Forms.TextBox();
            this.lblColorCode = new System.Windows.Forms.Label();
            this.lblUniversal = new System.Windows.Forms.Label();
            this.tboArtNummer = new System.Windows.Forms.TextBox();
            this.lblArticleNumber = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAmount10 = new System.Windows.Forms.Button();
            this.btnAmount9 = new System.Windows.Forms.Button();
            this.btnAmount7 = new System.Windows.Forms.Button();
            this.btnAmount4 = new System.Windows.Forms.Button();
            this.btnAmount6 = new System.Windows.Forms.Button();
            this.btnAmount3 = new System.Windows.Forms.Button();
            this.btnAmount2 = new System.Windows.Forms.Button();
            this.btnAmount5 = new System.Windows.Forms.Button();
            this.btnAmount8 = new System.Windows.Forms.Button();
            this.btnAmount1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tboSearchEanInput = new System.Windows.Forms.TextBox();
            this.lblResult = new System.Windows.Forms.ListBox();
            this.dgCurrentProduct = new System.Windows.Forms.DataGridView();
            this.tabSpecialHandling = new System.Windows.Forms.TabPage();
            this.comChooseComPort = new System.Windows.Forms.ComboBox();
            this.lblComInterface = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tabMainNavigation.SuspendLayout();
            this.tabPrintPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCurrentProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRecordCount.AutoSize = true;
            this.lblRecordCount.Location = new System.Drawing.Point(3, 647);
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(35, 13);
            this.lblRecordCount.TabIndex = 2;
            this.lblRecordCount.Text = "label1";
            // 
            // lblChoosenPrinterInterface
            // 
            this.lblChoosenPrinterInterface.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblChoosenPrinterInterface.AutoSize = true;
            this.lblChoosenPrinterInterface.Location = new System.Drawing.Point(3, 662);
            this.lblChoosenPrinterInterface.Name = "lblChoosenPrinterInterface";
            this.lblChoosenPrinterInterface.Size = new System.Drawing.Size(35, 13);
            this.lblChoosenPrinterInterface.TabIndex = 6;
            this.lblChoosenPrinterInterface.Text = "label4";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hilfeToolStripMenuItem,
            this.druckerToolStripMenuItem,
            this.dBToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1151, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualToolStripMenuItem,
            this.danielmuellercalidacomToolStripMenuItem});
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            // 
            // manualToolStripMenuItem
            // 
            this.manualToolStripMenuItem.Name = "manualToolStripMenuItem";
            this.manualToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.manualToolStripMenuItem.Text = "Etiketten Druck Hilfe";
            this.manualToolStripMenuItem.Click += new System.EventHandler(this.manualToolStripMenuItem_Click);
            // 
            // danielmuellercalidacomToolStripMenuItem
            // 
            this.danielmuellercalidacomToolStripMenuItem.Name = "danielmuellercalidacomToolStripMenuItem";
            this.danielmuellercalidacomToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.danielmuellercalidacomToolStripMenuItem.Text = "sdpmue@hotmail.com";
            // 
            // druckerToolStripMenuItem
            // 
            this.druckerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comToolStripMenuItem,
            this.tCPIPToolStripMenuItem});
            this.druckerToolStripMenuItem.Name = "druckerToolStripMenuItem";
            this.druckerToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.druckerToolStripMenuItem.Text = "Drucker";
            // 
            // comToolStripMenuItem
            // 
            this.comToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbxToolstripeChooseComport});
            this.comToolStripMenuItem.Name = "comToolStripMenuItem";
            this.comToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.comToolStripMenuItem.Text = "COM";
            this.comToolStripMenuItem.Click += new System.EventHandler(this.comToolStripMenuItem_Click);
            // 
            // cbxToolstripeChooseComport
            // 
            this.cbxToolstripeChooseComport.Name = "cbxToolstripeChooseComport";
            this.cbxToolstripeChooseComport.Size = new System.Drawing.Size(121, 23);
            this.cbxToolstripeChooseComport.SelectedIndexChanged += new System.EventHandler(this.cbxToolstripeChooseComport_SelectedIndexChanged);
            // 
            // tCPIPToolStripMenuItem
            // 
            this.tCPIPToolStripMenuItem.Name = "tCPIPToolStripMenuItem";
            this.tCPIPToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.tCPIPToolStripMenuItem.Text = "TCP/IP";
            this.tCPIPToolStripMenuItem.Click += new System.EventHandler(this.tCPIPToolStripMenuItem_Click);
            // 
            // dBToolStripMenuItem
            // 
            this.dBToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeDbPathToolStripMenuItem,
            this.excelAlsQuelleToolStripMenuItem});
            this.dBToolStripMenuItem.Name = "dBToolStripMenuItem";
            this.dBToolStripMenuItem.Size = new System.Drawing.Size(34, 20);
            this.dBToolStripMenuItem.Text = "DB";
            // 
            // changeDbPathToolStripMenuItem
            // 
            this.changeDbPathToolStripMenuItem.Name = "changeDbPathToolStripMenuItem";
            this.changeDbPathToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.changeDbPathToolStripMenuItem.Text = "Pfad ändern";
            this.changeDbPathToolStripMenuItem.Click += new System.EventHandler(this.changeDbPathToolStripMenuItem_Click);
            // 
            // excelAlsQuelleToolStripMenuItem
            // 
            this.excelAlsQuelleToolStripMenuItem.Name = "excelAlsQuelleToolStripMenuItem";
            this.excelAlsQuelleToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.excelAlsQuelleToolStripMenuItem.Text = "Excel als Quelle";
            this.excelAlsQuelleToolStripMenuItem.Click += new System.EventHandler(this.excelAlsQuelleToolStripMenuItem_Click);
            // 
            // lbl_hitCounter
            // 
            this.lbl_hitCounter.BackColor = System.Drawing.Color.GreenYellow;
            this.lbl_hitCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hitCounter.Location = new System.Drawing.Point(1049, 27);
            this.lbl_hitCounter.Name = "lbl_hitCounter";
            this.lbl_hitCounter.Size = new System.Drawing.Size(77, 18);
            this.lbl_hitCounter.TabIndex = 44;
            this.lbl_hitCounter.Text = "000000";
            this.lbl_hitCounter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_hitCounter.Visible = false;
            // 
            // TboIpAdresse
            // 
            this.TboIpAdresse.Location = new System.Drawing.Point(230, 2);
            this.TboIpAdresse.Name = "TboIpAdresse";
            this.TboIpAdresse.Size = new System.Drawing.Size(168, 20);
            this.TboIpAdresse.TabIndex = 45;
            this.TboIpAdresse.Leave += new System.EventHandler(this.tboIpAdresse_Leave);
            // 
            // lblIpAddress
            // 
            this.lblIpAddress.AutoSize = true;
            this.lblIpAddress.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblIpAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIpAddress.Location = new System.Drawing.Point(158, 2);
            this.lblIpAddress.Name = "lblIpAddress";
            this.lblIpAddress.Size = new System.Drawing.Size(65, 15);
            this.lblIpAddress.TabIndex = 46;
            this.lblIpAddress.Text = "Ip-Adresse";
            // 
            // lblCurrentDbPath
            // 
            this.lblCurrentDbPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentDbPath.AutoSize = true;
            this.lblCurrentDbPath.Location = new System.Drawing.Point(626, 660);
            this.lblCurrentDbPath.Name = "lblCurrentDbPath";
            this.lblCurrentDbPath.Size = new System.Drawing.Size(41, 13);
            this.lblCurrentDbPath.TabIndex = 47;
            this.lblCurrentDbPath.Text = "label14";
            // 
            // tabMainNavigation
            // 
            this.tabMainNavigation.Controls.Add(this.tabPrintPage);
            this.tabMainNavigation.Controls.Add(this.tabSpecialHandling);
            this.tabMainNavigation.Location = new System.Drawing.Point(6, 27);
            this.tabMainNavigation.Name = "tabMainNavigation";
            this.tabMainNavigation.SelectedIndex = 0;
            this.tabMainNavigation.Size = new System.Drawing.Size(1145, 621);
            this.tabMainNavigation.TabIndex = 48;
            // 
            // tabPrintPage
            // 
            this.tabPrintPage.BackColor = System.Drawing.Color.DarkGray;
            this.tabPrintPage.Controls.Add(this.tboWarning);
            this.tabPrintPage.Controls.Add(this.tboCurrency2);
            this.tabPrintPage.Controls.Add(this.tboCurrency1);
            this.tabPrintPage.Controls.Add(this.btnSetUserDefault);
            this.tabPrintPage.Controls.Add(this.lblDeclaration);
            this.tabPrintPage.Controls.Add(this.tboDeclaration);
            this.tabPrintPage.Controls.Add(this.lblPrice);
            this.tabPrintPage.Controls.Add(this.tboPrice);
            this.tabPrintPage.Controls.Add(this.tboArtDescription);
            this.tabPrintPage.Controls.Add(this.lblStoreName);
            this.tabPrintPage.Controls.Add(this.tboColor);
            this.tabPrintPage.Controls.Add(this.lblColor);
            this.tabPrintPage.Controls.Add(this.label9);
            this.tabPrintPage.Controls.Add(this.lblSize);
            this.tabPrintPage.Controls.Add(this.tboEan13);
            this.tabPrintPage.Controls.Add(this.tboSize);
            this.tabPrintPage.Controls.Add(this.tboColorCode);
            this.tabPrintPage.Controls.Add(this.lblColorCode);
            this.tabPrintPage.Controls.Add(this.lblUniversal);
            this.tabPrintPage.Controls.Add(this.tboArtNummer);
            this.tabPrintPage.Controls.Add(this.lblArticleNumber);
            this.tabPrintPage.Controls.Add(this.pictureBox1);
            this.tabPrintPage.Controls.Add(this.btnAmount10);
            this.tabPrintPage.Controls.Add(this.btnAmount9);
            this.tabPrintPage.Controls.Add(this.btnAmount7);
            this.tabPrintPage.Controls.Add(this.btnAmount4);
            this.tabPrintPage.Controls.Add(this.btnAmount6);
            this.tabPrintPage.Controls.Add(this.btnAmount3);
            this.tabPrintPage.Controls.Add(this.btnAmount2);
            this.tabPrintPage.Controls.Add(this.btnAmount5);
            this.tabPrintPage.Controls.Add(this.btnAmount8);
            this.tabPrintPage.Controls.Add(this.btnAmount1);
            this.tabPrintPage.Controls.Add(this.label2);
            this.tabPrintPage.Controls.Add(this.tboSearchEanInput);
            this.tabPrintPage.Controls.Add(this.lblResult);
            this.tabPrintPage.Controls.Add(this.dgCurrentProduct);
            this.tabPrintPage.Location = new System.Drawing.Point(4, 22);
            this.tabPrintPage.Name = "tabPrintPage";
            this.tabPrintPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrintPage.Size = new System.Drawing.Size(1137, 595);
            this.tabPrintPage.TabIndex = 0;
            this.tabPrintPage.Text = "Drucken";
            // 
            // tboWarning
            // 
            this.tboWarning.Font = new System.Drawing.Font("Arial Black", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboWarning.ForeColor = System.Drawing.Color.Red;
            this.tboWarning.Location = new System.Drawing.Point(637, 450);
            this.tboWarning.Multiline = true;
            this.tboWarning.Name = "tboWarning";
            this.tboWarning.Size = new System.Drawing.Size(283, 104);
            this.tboWarning.TabIndex = 79;
            this.tboWarning.Text = "warning text";
            this.tboWarning.Visible = false;
            // 
            // tboCurrency2
            // 
            this.tboCurrency2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboCurrency2.Location = new System.Drawing.Point(1034, 414);
            this.tboCurrency2.Name = "tboCurrency2";
            this.tboCurrency2.Size = new System.Drawing.Size(71, 26);
            this.tboCurrency2.TabIndex = 78;
            this.tboCurrency2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tboCurrency1
            // 
            this.tboCurrency1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboCurrency1.Location = new System.Drawing.Point(925, 216);
            this.tboCurrency1.Name = "tboCurrency1";
            this.tboCurrency1.Size = new System.Drawing.Size(71, 26);
            this.tboCurrency1.TabIndex = 77;
            this.tboCurrency1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnSetUserDefault
            // 
            this.btnSetUserDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetUserDefault.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSetUserDefault.Location = new System.Drawing.Point(927, 458);
            this.btnSetUserDefault.Name = "btnSetUserDefault";
            this.btnSetUserDefault.Size = new System.Drawing.Size(140, 89);
            this.btnSetUserDefault.TabIndex = 76;
            this.btnSetUserDefault.Text = "Set";
            this.btnSetUserDefault.UseVisualStyleBackColor = true;
            this.btnSetUserDefault.Click += new System.EventHandler(this.btnSetUserDefault_Click);
            // 
            // lblDeclaration
            // 
            this.lblDeclaration.AutoSize = true;
            this.lblDeclaration.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblDeclaration.Location = new System.Drawing.Point(668, 421);
            this.lblDeclaration.Name = "lblDeclaration";
            this.lblDeclaration.Size = new System.Drawing.Size(61, 13);
            this.lblDeclaration.TabIndex = 75;
            this.lblDeclaration.Text = "Deklaration";
            // 
            // tboDeclaration
            // 
            this.tboDeclaration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboDeclaration.Location = new System.Drawing.Point(742, 414);
            this.tboDeclaration.Name = "tboDeclaration";
            this.tboDeclaration.Size = new System.Drawing.Size(286, 26);
            this.tboDeclaration.TabIndex = 74;
            this.tboDeclaration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPrice.Location = new System.Drawing.Point(826, 307);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(30, 13);
            this.lblPrice.TabIndex = 72;
            this.lblPrice.Text = "Preis";
            // 
            // tboPrice
            // 
            this.tboPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboPrice.Location = new System.Drawing.Point(826, 314);
            this.tboPrice.MaxLength = 6;
            this.tboPrice.Name = "tboPrice";
            this.tboPrice.Size = new System.Drawing.Size(279, 98);
            this.tboPrice.TabIndex = 73;
            // 
            // tboArtDescription
            // 
            this.tboArtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboArtDescription.Location = new System.Drawing.Point(635, 216);
            this.tboArtDescription.Name = "tboArtDescription";
            this.tboArtDescription.Size = new System.Drawing.Size(285, 26);
            this.tboArtDescription.TabIndex = 62;
            // 
            // lblStoreName
            // 
            this.lblStoreName.AutoSize = true;
            this.lblStoreName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblStoreName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStoreName.Location = new System.Drawing.Point(630, 158);
            this.lblStoreName.Name = "lblStoreName";
            this.lblStoreName.Size = new System.Drawing.Size(27, 29);
            this.lblStoreName.TabIndex = 71;
            this.lblStoreName.Text = "_";
            // 
            // tboColor
            // 
            this.tboColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboColor.Location = new System.Drawing.Point(636, 260);
            this.tboColor.Name = "tboColor";
            this.tboColor.Size = new System.Drawing.Size(362, 26);
            this.tboColor.TabIndex = 70;
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblColor.Location = new System.Drawing.Point(633, 245);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(34, 13);
            this.lblColor.TabIndex = 69;
            this.lblColor.Text = "Farbe";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(632, 547);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 13);
            this.label9.TabIndex = 68;
            this.label9.Text = "EAN-Code 13 Stellen";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSize.Location = new System.Drawing.Point(634, 307);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(40, 13);
            this.lblSize.TabIndex = 67;
            this.lblSize.Text = "Grösse";
            // 
            // tboEan13
            // 
            this.tboEan13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboEan13.Location = new System.Drawing.Point(635, 560);
            this.tboEan13.MaxLength = 13;
            this.tboEan13.Name = "tboEan13";
            this.tboEan13.Size = new System.Drawing.Size(363, 29);
            this.tboEan13.TabIndex = 66;
            // 
            // tboSize
            // 
            this.tboSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboSize.Location = new System.Drawing.Point(635, 314);
            this.tboSize.MaxLength = 4;
            this.tboSize.Name = "tboSize";
            this.tboSize.Size = new System.Drawing.Size(185, 98);
            this.tboSize.TabIndex = 65;
            // 
            // tboColorCode
            // 
            this.tboColorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboColorCode.Location = new System.Drawing.Point(1004, 233);
            this.tboColorCode.Name = "tboColorCode";
            this.tboColorCode.Size = new System.Drawing.Size(103, 29);
            this.tboColorCode.TabIndex = 64;
            this.tboColorCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblColorCode
            // 
            this.lblColorCode.AutoSize = true;
            this.lblColorCode.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblColorCode.Location = new System.Drawing.Point(1001, 220);
            this.lblColorCode.Name = "lblColorCode";
            this.lblColorCode.Size = new System.Drawing.Size(56, 13);
            this.lblColorCode.TabIndex = 63;
            this.lblColorCode.Text = "Farb-Code";
            // 
            // lblUniversal
            // 
            this.lblUniversal.AutoSize = true;
            this.lblUniversal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblUniversal.Location = new System.Drawing.Point(632, 197);
            this.lblUniversal.Name = "lblUniversal";
            this.lblUniversal.Size = new System.Drawing.Size(51, 13);
            this.lblUniversal.TabIndex = 61;
            this.lblUniversal.Text = "Universal";
            // 
            // tboArtNummer
            // 
            this.tboArtNummer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboArtNummer.Location = new System.Drawing.Point(854, 184);
            this.tboArtNummer.Name = "tboArtNummer";
            this.tboArtNummer.Size = new System.Drawing.Size(251, 29);
            this.tboArtNummer.TabIndex = 60;
            this.tboArtNummer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblArticleNumber
            // 
            this.lblArticleNumber.AutoSize = true;
            this.lblArticleNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblArticleNumber.Location = new System.Drawing.Point(851, 170);
            this.lblArticleNumber.Name = "lblArticleNumber";
            this.lblArticleNumber.Size = new System.Drawing.Size(78, 13);
            this.lblArticleNumber.TabIndex = 59;
            this.lblArticleNumber.Text = "Artikel-Nummer";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(618, 157);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(498, 442);
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            // 
            // btnAmount10
            // 
            this.btnAmount10.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount10.Location = new System.Drawing.Point(471, 390);
            this.btnAmount10.Name = "btnAmount10";
            this.btnAmount10.Size = new System.Drawing.Size(133, 104);
            this.btnAmount10.TabIndex = 57;
            this.btnAmount10.Text = "10";
            this.btnAmount10.UseVisualStyleBackColor = true;
            this.btnAmount10.Click += new System.EventHandler(this.btnAmount10_Click);
            // 
            // btnAmount9
            // 
            this.btnAmount9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount9.Location = new System.Drawing.Point(323, 390);
            this.btnAmount9.Name = "btnAmount9";
            this.btnAmount9.Size = new System.Drawing.Size(133, 104);
            this.btnAmount9.TabIndex = 56;
            this.btnAmount9.Text = "9";
            this.btnAmount9.UseVisualStyleBackColor = true;
            this.btnAmount9.Click += new System.EventHandler(this.btnAmount9_Click);
            // 
            // btnAmount7
            // 
            this.btnAmount7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount7.Location = new System.Drawing.Point(471, 273);
            this.btnAmount7.Name = "btnAmount7";
            this.btnAmount7.Size = new System.Drawing.Size(133, 104);
            this.btnAmount7.TabIndex = 55;
            this.btnAmount7.Text = "7";
            this.btnAmount7.UseVisualStyleBackColor = true;
            this.btnAmount7.Click += new System.EventHandler(this.btnAmount7_Click);
            // 
            // btnAmount4
            // 
            this.btnAmount4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount4.Location = new System.Drawing.Point(471, 155);
            this.btnAmount4.Name = "btnAmount4";
            this.btnAmount4.Size = new System.Drawing.Size(133, 104);
            this.btnAmount4.TabIndex = 54;
            this.btnAmount4.Text = "4";
            this.btnAmount4.UseVisualStyleBackColor = true;
            this.btnAmount4.Click += new System.EventHandler(this.btnAmount4_Click);
            // 
            // btnAmount6
            // 
            this.btnAmount6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount6.Location = new System.Drawing.Point(323, 273);
            this.btnAmount6.Name = "btnAmount6";
            this.btnAmount6.Size = new System.Drawing.Size(133, 104);
            this.btnAmount6.TabIndex = 53;
            this.btnAmount6.Text = "6";
            this.btnAmount6.UseVisualStyleBackColor = true;
            this.btnAmount6.Click += new System.EventHandler(this.btnAmonut6_Click);
            // 
            // btnAmount3
            // 
            this.btnAmount3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount3.Location = new System.Drawing.Point(323, 155);
            this.btnAmount3.Name = "btnAmount3";
            this.btnAmount3.Size = new System.Drawing.Size(133, 104);
            this.btnAmount3.TabIndex = 52;
            this.btnAmount3.Text = "3";
            this.btnAmount3.UseVisualStyleBackColor = true;
            this.btnAmount3.Click += new System.EventHandler(this.btnAmount3_Click);
            // 
            // btnAmount2
            // 
            this.btnAmount2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount2.Location = new System.Drawing.Point(178, 155);
            this.btnAmount2.Name = "btnAmount2";
            this.btnAmount2.Size = new System.Drawing.Size(133, 104);
            this.btnAmount2.TabIndex = 51;
            this.btnAmount2.Text = "2";
            this.btnAmount2.UseVisualStyleBackColor = true;
            this.btnAmount2.Click += new System.EventHandler(this.btnAmount2_Click);
            // 
            // btnAmount5
            // 
            this.btnAmount5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount5.Location = new System.Drawing.Point(178, 273);
            this.btnAmount5.Name = "btnAmount5";
            this.btnAmount5.Size = new System.Drawing.Size(133, 104);
            this.btnAmount5.TabIndex = 50;
            this.btnAmount5.Text = "5";
            this.btnAmount5.UseVisualStyleBackColor = true;
            this.btnAmount5.Click += new System.EventHandler(this.btnAmount5_Click);
            // 
            // btnAmount8
            // 
            this.btnAmount8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount8.Location = new System.Drawing.Point(178, 390);
            this.btnAmount8.Name = "btnAmount8";
            this.btnAmount8.Size = new System.Drawing.Size(133, 104);
            this.btnAmount8.TabIndex = 49;
            this.btnAmount8.Text = "8";
            this.btnAmount8.UseVisualStyleBackColor = true;
            this.btnAmount8.Click += new System.EventHandler(this.btnAmount8_Click);
            // 
            // btnAmount1
            // 
            this.btnAmount1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAmount1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAmount1.Location = new System.Drawing.Point(178, 500);
            this.btnAmount1.Name = "btnAmount1";
            this.btnAmount1.Size = new System.Drawing.Size(429, 89);
            this.btnAmount1.TabIndex = 48;
            this.btnAmount1.Text = "Drucken";
            this.btnAmount1.UseVisualStyleBackColor = true;
            this.btnAmount1.Click += new System.EventHandler(this.btnAmount1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 46);
            this.label2.TabIndex = 47;
            this.label2.Text = "EAN-Code";
            // 
            // tboSearchEanInput
            // 
            this.tboSearchEanInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboSearchEanInput.Location = new System.Drawing.Point(224, 8);
            this.tboSearchEanInput.Name = "tboSearchEanInput";
            this.tboSearchEanInput.Size = new System.Drawing.Size(896, 40);
            this.tboSearchEanInput.TabIndex = 46;
            this.tboSearchEanInput.Click += new System.EventHandler(this.tboSearchEanInput_Click);
            this.tboSearchEanInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tboSearchEanInput_KeyUp);
            // 
            // lblResult
            // 
            this.lblResult.BackColor = System.Drawing.Color.GreenYellow;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.FormattingEnabled = true;
            this.lblResult.ItemHeight = 20;
            this.lblResult.Location = new System.Drawing.Point(224, 52);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(896, 104);
            this.lblResult.TabIndex = 45;
            this.lblResult.Visible = false;
            this.lblResult.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblResult_MouseDoubleClick);
            // 
            // dgCurrentProduct
            // 
            this.dgCurrentProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCurrentProduct.Enabled = false;
            this.dgCurrentProduct.Location = new System.Drawing.Point(14, 57);
            this.dgCurrentProduct.Name = "dgCurrentProduct";
            this.dgCurrentProduct.Size = new System.Drawing.Size(1106, 90);
            this.dgCurrentProduct.TabIndex = 44;
            // 
            // tabSpecialHandling
            // 
            this.tabSpecialHandling.BackColor = System.Drawing.Color.DarkGray;
            this.tabSpecialHandling.Location = new System.Drawing.Point(4, 22);
            this.tabSpecialHandling.Name = "tabSpecialHandling";
            this.tabSpecialHandling.Padding = new System.Windows.Forms.Padding(3);
            this.tabSpecialHandling.Size = new System.Drawing.Size(1137, 595);
            this.tabSpecialHandling.TabIndex = 1;
            this.tabSpecialHandling.Text = "Sonderbearbeitung";
            // 
            // comChooseComPort
            // 
            this.comChooseComPort.FormattingEnabled = true;
            this.comChooseComPort.Location = new System.Drawing.Point(1049, 2);
            this.comChooseComPort.Name = "comChooseComPort";
            this.comChooseComPort.Size = new System.Drawing.Size(77, 21);
            this.comChooseComPort.TabIndex = 5;
            this.comChooseComPort.Visible = false;
            // 
            // lblComInterface
            // 
            this.lblComInterface.AutoSize = true;
            this.lblComInterface.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblComInterface.Location = new System.Drawing.Point(910, 5);
            this.lblComInterface.Name = "lblComInterface";
            this.lblComInterface.Size = new System.Drawing.Size(127, 13);
            this.lblComInterface.TabIndex = 8;
            this.lblComInterface.Text = "COM-Druckerschnittstelle";
            this.lblComInterface.Visible = false;
            // 
            // frmMainStartup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1151, 675);
            this.Controls.Add(this.lblCurrentDbPath);
            this.Controls.Add(this.lblIpAddress);
            this.Controls.Add(this.TboIpAdresse);
            this.Controls.Add(this.lbl_hitCounter);
            this.Controls.Add(this.lblComInterface);
            this.Controls.Add(this.lblChoosenPrinterInterface);
            this.Controls.Add(this.comChooseComPort);
            this.Controls.Add(this.lblRecordCount);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabMainNavigation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMainStartup";
            this.Text = "frmMainStartup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            //this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabMainNavigation.ResumeLayout(false);
            this.tabPrintPage.ResumeLayout(false);
            this.tabPrintPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCurrentProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRecordCount;
        private System.Windows.Forms.Label lblChoosenPrinterInterface;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danielmuellercalidacomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem;
        private System.Windows.Forms.Label lbl_hitCounter;
        private System.Windows.Forms.ToolStripMenuItem druckerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cbxToolstripeChooseComport;
        private System.Windows.Forms.ToolStripMenuItem tCPIPToolStripMenuItem;
        private System.Windows.Forms.TextBox TboIpAdresse;
        private System.Windows.Forms.Label lblIpAddress;
        private System.Windows.Forms.ToolStripMenuItem dBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeDbPathToolStripMenuItem;
        private System.Windows.Forms.Label lblCurrentDbPath;
        private System.Windows.Forms.TabControl tabMainNavigation;
        private System.Windows.Forms.TabPage tabPrintPage;
        private System.Windows.Forms.TabPage tabSpecialHandling;
        private System.Windows.Forms.TextBox tboWarning;
        private System.Windows.Forms.TextBox tboCurrency2;
        private System.Windows.Forms.TextBox tboCurrency1;
        private System.Windows.Forms.Button btnSetUserDefault;
        private System.Windows.Forms.Label lblDeclaration;
        private System.Windows.Forms.TextBox tboDeclaration;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox tboPrice;
        private System.Windows.Forms.TextBox tboArtDescription;
        private System.Windows.Forms.Label lblStoreName;
        private System.Windows.Forms.TextBox tboColor;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.TextBox tboEan13;
        private System.Windows.Forms.TextBox tboSize;
        private System.Windows.Forms.TextBox tboColorCode;
        private System.Windows.Forms.Label lblColorCode;
        private System.Windows.Forms.Label lblUniversal;
        private System.Windows.Forms.TextBox tboArtNummer;
        private System.Windows.Forms.Label lblArticleNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAmount10;
        private System.Windows.Forms.Button btnAmount9;
        private System.Windows.Forms.Button btnAmount7;
        private System.Windows.Forms.Button btnAmount4;
        private System.Windows.Forms.Button btnAmount6;
        private System.Windows.Forms.Button btnAmount3;
        private System.Windows.Forms.Button btnAmount2;
        private System.Windows.Forms.Button btnAmount5;
        private System.Windows.Forms.Button btnAmount8;
        private System.Windows.Forms.Button btnAmount1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tboSearchEanInput;
        private System.Windows.Forms.ListBox lblResult;
        private System.Windows.Forms.DataGridView dgCurrentProduct;
        private System.Windows.Forms.ComboBox comChooseComPort;
        private System.Windows.Forms.Label lblComInterface;
        private System.Windows.Forms.ToolStripMenuItem excelAlsQuelleToolStripMenuItem;
    }
}

