﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SpecialHandlingEtikettenDruckV3
{
    public partial class frmMainStartup : Form
    {
        public frmMainStartup()
        {
            InitializeComponent();
 
            frmChooseDatabase.SetDatabaseCurrency += DatabaseManagement.SetDatabaseName;
            frmChooseDatabase.ActionAfterImDone += ActionAfterImDone;
            DatabaseManagement.ToggleCursor += ToggleCursor;
            DatabaseManagement.SendCurrentDbPath += SetDataBasePath;
            DatabaseManagement.SendRecordCount += SendRecordCount;
            DatabaseManagement.SendMainFormTitle += SendMainFormTitle;
            DatabaseManagement.SendWarningText += SendWarningText;
            ComInterfaceHandler.InterfaceChanged += InterfaceChanged;
            LabelStyleHandler.StorenameDescriptionChanged += StorenameDescriptionChanged;
            LabelStyleHandler.StoreTitleChanged += StoreTitleChanged;
            SearchEngine.SendData += SendData;
            SearchEngine.SetMember += SetMember;
            PrintHandler.SetWarning += SetWarning;
            PrintHandler.SetCom += SetCom;
            PrintHandler.SetTcpIp += SetTcpIp;


            GetComPortList();
            MainConfig.mainAppConfiguration.Init = true;
            SetComboboxComportIndex();
            MainConfig.mainAppConfiguration.IpAddress = AppSettingHandler.GetAppSetting(SettingNames.IpAddress.ToString());
            TboIpAdresse.Text = MainConfig.mainAppConfiguration.IpAddress;
            PrintHandler.PrintModeChange(PrinterInterface.Unknown);
            SetFormTitle();
            if (MainConfig.conf.Waehrung == Currency.ChfEur) { this.WindowState = FormWindowState.Minimized; }
            else { this.WindowState = FormWindowState.Maximized; }
            DatabaseManagement.CheckCurrentDbPath(false);
            ActiveControl = tboSearchEanInput;
        }
        
        CalidaArticle GetArticle(string eanNumber)
        {
            CalidaArticle result = new CalidaArticle();
            if (eanNumber.Length == 13 || eanNumber.StartsWith(","))
            {
                List<CalidaArticle> res = new List<CalidaArticle>(); ;
                res = SearchEngine.SearchArticleInAssortment(eanNumber);
                if (res.Count() > 0)
                {
                    MainConfig.mainAppConfiguration.LastPrint = res.First();
                    var resultList = res.ToList();
                    dgCurrentProduct.DataSource = resultList;
                    dgCurrentProduct.AutoResizeColumns();
                    string labelContent = LabelStyleHandler.GetNewLabel(res.First(), MainConfig.conf.LabelStyle);       
                    PrintHandler.PrintLabel(labelContent);
                    DisplayLabel(res.First());
                }
                else
                {                    
                        MessageBox.Show("Es konnte kein Artikel gefunden werden");
                }
                EanTextBoxRefresh();
            }
            return result;
        }

        void ToggleCursor(bool onOff) {
            if (!onOff) { this.Cursor = Cursors.Default; }
            if (onOff) { this.Cursor = Cursors.WaitCursor; }
        }
        void DisplayLabel(CalidaArticle res)
        {
            switch (MainConfig.conf.LabelStyle)
            {
                case LabelMode.Zalando:
                    DisplayZalando(res);
                    break;

                case LabelMode.Retouren:
                    DisplayRetouren(res);
                    break;

                case LabelMode.ManuellesLager:
                    DisplayRetouren(res);
                    break;

                case LabelMode.Zweitwahl:
                    DisplayZweitwahl(res);
                    break;

                case LabelMode.CalidaStore:
                    DisplayCalidaStore(res);
                    break;

                case LabelMode.Ottoversand:
                    DisplayOttoVersand(res);
                    break;
            }
        }
        void SetFormTitle() {
            var versionAttribute = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true).FirstOrDefault() as AssemblyFileVersionAttribute;
            if (versionAttribute != null)
            {
                MainConfig.mainAppConfiguration.AssemblyVersion = versionAttribute.Version;
                this.Text = MainConfig.GetFormTitle(MainConfig.mainAppConfiguration.AssemblyVersion);
            }
            else { this.Text = MainConfig.mainAppConfiguration.FrmTitle; }
        }
        void GetComPortList() {
            string[] ports = SerialPort.GetPortNames();
            cbxToolstripeChooseComport.ComboBox.BindingContext = this.BindingContext;
            cbxToolstripeChooseComport.ComboBox.DataSource = ports.ToList();
        }
        void DisplayZalando(CalidaArticle res) {
            tboArtNummer.Text = res.ArtNummer.ToString();
            tboArtDescription.Text = res.ArtikelBeschreibung.ToString();
            tboColorCode.Text = res.Color.ToString();
            tboColor.Text = res.FarbBezeichnung.ToString();
            tboSize.Text = res.Size.ToString();
            tboEan13.Text = res.Scanned.ToString();
        }
        void DisplayRetouren(CalidaArticle res) {
            string demodPreis = "***,**";
            if (res.DemodPreis > 0) { demodPreis = String.Format("{0:0.00}", res.DemodPreis); }
            tboArtNummer.Text = res.ArtNummer.ToString();
            tboArtDescription.Text = String.Format("{0:0.00}", res.VerkaufsPreis);//res.First().VerkaufsPreis.ToString();// + " " + res.First().Waehrung;
            tboColorCode.Text = res.Color.ToString();
            tboColor.Text = "";//res.First().FarbBezeichnung.ToString();
            tboSize.Text = res.Size.ToString();
            tboPrice.Text = String.Format("{0:0.00}", res.DemodPreis);//res.First().DemodPreis);
            tboDeclaration.Text = "unverb. Richtpreis ";// + res.First().Waehrung;
            tboCurrency1.Text = res.Waehrung;
            tboCurrency2.Text = res.Waehrung;
            tboEan13.Text = res.Scanned.ToString();
        }
        void DisplayZweitwahl(CalidaArticle res) {
            string ZweitWahlPreis = "***,**";
            if (res.ZweitWahlPreis > 0) { ZweitWahlPreis = String.Format("{0:0.00}", res.ZweitWahlPreis); }
            tboArtNummer.Text = res.ArtNummer.ToString();
            tboArtDescription.Text = String.Format("{0:0.00}", res.VerkaufsPreis);//res.First().VerkaufsPreis.ToString();// + " " + res.First().Waehrung;
            tboColorCode.Text = res.Color.ToString();
            tboColor.Text = "";//res.First().FarbBezeichnung.ToString();
            tboSize.Text = res.Size.ToString();
            tboPrice.Text = String.Format("{0:0.00}", res.ZweitWahlPreis);//res.First().DemodPreis);
            tboDeclaration.Text = "unverb. Richtpreis ";// + res.First().Waehrung;
            tboCurrency1.Text = res.Waehrung;
            tboCurrency2.Text = res.Waehrung;
            tboEan13.Text = res.EanNr2.ToString();
        }
        void DisplayCalidaStore(CalidaArticle res) {
            tboArtNummer.Text = res.ArtNummer.ToString();
            tboArtDescription.Text = MainConfig.conf.StoreLocation;
            tboColorCode.Text = res.Color.ToString();
            tboColor.Text = "";
            tboSize.Text = res.Size.ToString();
            tboPrice.Text = String.Format("{0:0.00}", res.VerkaufsPreis);
            tboDeclaration.Text = "unverb. Richtpreis ";
            tboCurrency1.Text = res.Waehrung;
            tboCurrency2.Text = res.Waehrung;
            tboEan13.Text = res.Scanned.ToString();
        }
        void DisplayOttoVersand(CalidaArticle res) {
            tboArtDescription.Text = res.OttoVersandDeEigeneArtikelnummer;
            tboColor.Text = "";
            tboSize.Text = res.Size.ToString();
            tboEan13.Text = res.Scanned.ToString();
        }
        void ActionAfterImDone()
        {
            this.WindowState = FormWindowState.Maximized;
        }
        void SetDataBasePath(string dbName)
        {
            lblCurrentDbPath.Text = dbName;
        }
        void SendRecordCount(string amount)
        {
            lblRecordCount.Text = amount;
        }
        void SendMainFormTitle(string title)
        {
            this.Text = title;
        }
        void InterfaceChanged(string newInterface)
        {
            lblChoosenPrinterInterface.Text = newInterface;
        }
        void SetCom() {
            comToolStripMenuItem.Checked = true;
            tCPIPToolStripMenuItem.Checked = false;
            lblIpAddress.Visible = false;
            TboIpAdresse.Visible = false;
        }
        void SetTcpIp() {
            comToolStripMenuItem.Checked = false;
            tCPIPToolStripMenuItem.Checked = true;
            lblChoosenPrinterInterface.Text = "Druckerschnittstelle: Tcp/Ip" + MainConfig.mainAppConfiguration.IpAddress;
            lblIpAddress.Visible = true;
            TboIpAdresse.Visible = true;
        }
        void StorenameDescriptionChanged(string storeName)
        {
            tboArtDescription.Text = storeName;
        }
        void StoreTitleChanged(string newTitle)
        {
            lblStoreName.Text = newTitle;
        }
        void SendWarningText(string warningText)
        {
            tboWarning.Text = warningText;
        }
        void SetWarning(bool state) {
            tboWarning.Visible = state;
        }
        void SendData(object data)
        {
            lblResult.DataSource = data;
        }
        void SetMember(string member)
        {
            lblResult.DisplayMember = member;
        }
        void CheckTextboxState()
        {
            if (tboSearchEanInput.Text.StartsWith(","))
            {
                tboSearchEanInput.BackColor = Color.GreenYellow;
                lblResult.Visible = true;
                lbl_hitCounter.Visible = true;
                lbl_hitCounter.Text = SearchEngine.GetEanThroughSelection(tboSearchEanInput.Text).ToString();
            }
            else
            {
                tboSearchEanInput.BackColor = Color.White;
                lblResult.Visible = false;
                lbl_hitCounter.Visible = false;
            }
        }
        void EanTextBoxRefresh()
        {
            tboSearchEanInput.Text = MainConfig.mainAppConfiguration.TboDefaultEan;
            tboSearchEanInput.Focus();
            tboSearchEanInput.SelectAll();
        }
        void SetButtonStyle(object button)
        {
            Button btn = new Button();
            if (button is Button)
            {
                btn = (Button)button;// = Color.DarkBlue;
                btn.BackColor = Color.DarkBlue;
                PrintHandler.PrintLabel(LabelStyleHandler.GetNewLabel(MainConfig.mainAppConfiguration.LastPrint, MainConfig.conf.LabelStyle));
                tboSearchEanInput.Focus();
                tboSearchEanInput.SelectAll();
                MainConfig.mainAppConfiguration.ActAmount = 1;
                ResetButtons(0);
            }
        }
        void ResetColor(Button sender)
        {
            if (sender is Button) { Button btn = (Button)sender; btn.BackColor = SystemColors.ActiveBorder; }
        }
        void ResetButtons(int amount)
        {
            if (amount == 0 || amount <= 0) { amount = 1; }
            MainConfig.mainAppConfiguration.ActAmount = amount;

            ResetColor(btnAmount2);
            ResetColor(btnAmount3);
            ResetColor(btnAmount4);
            ResetColor(btnAmount5);
            ResetColor(btnAmount6);
            ResetColor(btnAmount7);
            ResetColor(btnAmount8);
            ResetColor(btnAmount9);
            ResetColor(btnAmount10);
            ResetColor(btnAmount1);
        }
        void SetComboboxComportIndex()
        {
            var indexNr = cbxToolstripeChooseComport.ComboBox.Items.IndexOf(AppSettingHandler.GetAppSetting(SettingNames.ComPort.ToString()));
            if (indexNr != -1)
            {
                cbxToolstripeChooseComport.ComboBox.SelectedIndex = cbxToolstripeChooseComport.ComboBox.Items.IndexOf(AppSettingHandler.GetAppSetting(SettingNames.ComPort.ToString()));
            }
        }

        //Steuerelemente
        private void tboSearchEanInput_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (tboSearchEanInput.Text.Length == 13)
                {
                    GetArticle(tboSearchEanInput.Text);
                }
                else if (tboSearchEanInput.Text.StartsWith(","))
                {
                    CalidaArticle res = new CalidaArticle();
                    try {
                        res = (CalidaArticle)lblResult.SelectedItem;
                        if (res != null) { GetArticle(res.EanNr1); };
                    }
                    catch(Exception ex){
                        var t = ex.Message;
                    }
                }
                else
                {
                    EanTextBoxRefresh();
                }

            CheckTextboxState();
        }
        private void lblResult_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CalidaArticle calArt = new CalidaArticle();
            try
            {
                calArt=(CalidaArticle)lblResult.SelectedValue;
                GetArticle(calArt.EanNr1);
                CheckTextboxState();
            }
            catch (Exception ex) {
                var t = ex.Message;
            }
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            EanTextBoxRefresh();
        }
        private void cbxToolstripeChooseComport_SelectedIndexChanged(object sender, EventArgs e)
        {
            string comPortProperty = cbxToolstripeChooseComport.Text;
            if (MainConfig.mainAppConfiguration.Init && comPortProperty.ToUpper().Contains("COM"))
            {
                AppSettingHandler.SetAppSetting(SettingNames.ComPort.ToString(), cbxToolstripeChooseComport.Text);
            }
            ComInterfaceHandler.ComOpenPort(cbxToolstripeChooseComport.Text);
        }
        private void Form1_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            MainConfig.mainAppConfiguration.ComPort.Close();
        }
        private void tboSearchEanInput_Click(object sender, EventArgs e)
        {
            tboSearchEanInput.SelectAll();
        }
        private void btnAmount1_Click(object sender, EventArgs e)
        {
            ResetButtons(1);
            SetButtonStyle(sender);
            tboSearchEanInput.Focus();
            tboSearchEanInput.SelectAll();
        }
        private void btnSetUserDefault_Click(object sender, EventArgs e)
        {
            double artikelNummer = 0;
            double colorCode = 0;
            double demodPreis = 0;
            double verkaufsPreis = 0;

            Double.TryParse(tboArtNummer.Text, out artikelNummer);
            Double.TryParse(tboColorCode.Text, out colorCode);
            if (!Double.TryParse(tboArtDescription.Text, out demodPreis))
            {

            }
            Double.TryParse(tboPrice.Text, out verkaufsPreis);

            if ((verkaufsPreis > 0) || (demodPreis > 0))
            {
                if (demodPreis > verkaufsPreis)
                {
                    double temp = verkaufsPreis;
                    verkaufsPreis = demodPreis;
                    demodPreis = temp;
                }
            }
            CalidaArticle calArt = new CalidaArticle
            {
                ArtikelBeschreibung = tboArtDescription.Text
                ,
                ArtNummer = artikelNummer
                ,
                Color = colorCode
                ,
                DemodPreis = demodPreis
                ,
                EanNr1 = tboEan13.Text
                ,
                EanNr2 = ""
                ,
                FarbBezeichnung = tboColor.Text
                ,
                Kollektion = ""
                ,
                Scanned = tboEan13.Text
                ,
                Size = tboSize.Text
                ,
                VerkaufsPreis = verkaufsPreis
                ,
                Waehrung = tboCurrency1.Text
                ,
                Deklaration = tboDeclaration.Text
                ,
                OttoVersandDeEigeneArtikelnummer = tboArtDescription.Text
            };

            if (MainConfig.conf.AllowUserDefinedLabel == true)
            {
                PrintHandler.PrintLabel(LabelStyleHandler.GetNewLabel(calArt, MainConfig.conf.LabelStyle));
                MainConfig.mainAppConfiguration.LastPrint = calArt;
            }
            else
            {
                MessageBox.Show("Sie haben keine Berechtigung benutzerdefinierte Etiketten zu erstellen.");
            }
            tboSearchEanInput.Focus();
            tboSearchEanInput.SelectAll();
        }
        private void btnAmount2_Click(object sender, EventArgs e)
        {
            ResetButtons(2);
            SetButtonStyle(sender);
        }
        private void btnAmount3_Click(object sender, EventArgs e)
        {
            ResetButtons(3);
            SetButtonStyle(sender);
        }
        private void btnAmount4_Click(object sender, EventArgs e)
        {
            ResetButtons(4);
            SetButtonStyle(sender);
        }
        private void btnAmount5_Click(object sender, EventArgs e)
        {
            ResetButtons(5);
            SetButtonStyle(sender);
        }
        private void btnAmonut6_Click(object sender, EventArgs e)
        {
            ResetButtons(6);
            SetButtonStyle(sender);
        }
        private void btnAmount7_Click(object sender, EventArgs e)
        {
            ResetButtons(7);
            SetButtonStyle(sender);
        }
        private void btnAmount8_Click(object sender, EventArgs e)
        {
            ResetButtons(8);
            SetButtonStyle(sender);
        }
        private void btnAmount9_Click(object sender, EventArgs e)
        {
            ResetButtons(9);
            SetButtonStyle(sender);
        }
        private void btnAmount10_Click(object sender, EventArgs e)
        {
            ResetButtons(10);
            SetButtonStyle(sender);
        }
        private void manualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = Path.Combine(MainConfig.mainAppConfiguration.MyFolder, "Etiketten_Systemhandbuch.mht");
            if (File.Exists(path))
            {
                string target = path;
                //Use no more than one assignment when you test this code. 
                //string target = "ftp://ftp.microsoft.com";
                //string target = "C:\\Program Files\\Microsoft Visual Studio\\INSTALL.HTM"; 

                try
                {
                    System.Diagnostics.Process.Start(target);
                }
                catch
                    (
                     System.ComponentModel.Win32Exception noBrowser)
                {
                    if (noBrowser.ErrorCode == -2147467259)
                        MessageBox.Show(noBrowser.Message);
                }
                catch (System.Exception other)
                {
                    MessageBox.Show(other.Message);
                }
            }
        }
        private void comToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintHandler.PrintModeChange(PrinterInterface.Com);
        }
        private void tCPIPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintHandler.PrintModeChange(PrinterInterface.TcpIp);
        }
        private void tboIpAdresse_Leave(object sender, EventArgs e)
        {
            AppSettingHandler.SetAppSetting(SettingNames.IpAddress.ToString(), TboIpAdresse.Text);
            MainConfig.mainAppConfiguration.IpAddress = AppSettingHandler.GetAppSetting(SettingNames.IpAddress.ToString());
            PrintHandler.PrintModeChange(PrinterInterface.TcpIp);
        }
        private void changeDbPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseManagement.CheckCurrentDbPath(true);
        }
        private void lblResult_MouseDoubleClick(object sender, EventArgs e)
        {
            CalidaArticle calArt = (CalidaArticle)lblResult.SelectedValue;
            GetArticle(calArt.EanNr1);
            CheckTextboxState();
        }
        private void excelAlsQuelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseManagement.LoadDatabase("Excel", MainConfig.mainAppConfiguration.SortimentTableName);
        }
    }
}
