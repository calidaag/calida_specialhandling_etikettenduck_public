﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    class AccessHandler
    {
        public static DataTable GetArticleTableFromAccess(string dbName, string tableName)
        {
            DataTable article = new DataTable();
            try
            {
                OleDbConnection conn = new OleDbConnection();
                String connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Path.Combine(MainConfig.mainAppConfiguration.MyFolder, dbName) + ";Persist Security Info=True";
                string sql = "Select * from " + tableName;
                conn.ConnectionString = connection;
                conn.Open();
                DataSet ds = new DataSet();
                OleDbDataAdapter adapter = new OleDbDataAdapter(sql, conn);
                adapter.Fill(ds);
                article = ds.Tables["Table"];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return article;
        }
    }
}
