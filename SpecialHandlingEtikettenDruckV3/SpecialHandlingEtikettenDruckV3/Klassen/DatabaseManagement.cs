﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Data;

namespace SpecialHandlingEtikettenDruckV3
{
    class DatabaseManagement
    {
        public delegate void CursorWaitEnable(bool onOff);
        public static event CursorWaitEnable ToggleCursor;

        public delegate void SubmitInfo(string msg);
        public static event SubmitInfo SendCurrentDbPath;
        public static event SubmitInfo SendRecordCount;
        public static event SubmitInfo SendMainFormTitle;
        public static event SubmitInfo SendWarningText;

        public static double CheckIfDataSourceIsValid()
        {
            List<KeyValuePair<string, object>> mandatoryColums = MainConfig.GetMandatoryColumsForSearchEngine();
            string errMsg = "";
            foreach (KeyValuePair<string, object> kp in mandatoryColums)
            {
                Type curType = null;
                if (MainConfig.mainAppConfiguration.Article.Columns.Contains(kp.Key)) { curType = MainConfig.mainAppConfiguration.Article.Columns[kp.Key].DataType; }
                Type expectedType = kp.Value.GetType();
                if (curType == null)
                {
                    MainConfig.mainAppConfiguration.Article.Columns.Add(kp.Key, expectedType);
                    if (kp.Key.ToLower() == "ottoartikel" && MainConfig.conf.LabelStyle == LabelMode.Ottoversand) { AddOttoArticleNumberToDataTable(); }

                    #region FakeOttoNumber
                    // !!! Only for test. As soon the Database ist avaliable this code must be commented out!!!
                    /* // insert a randomized faked number in each record.
                    Random r = new Random();
                    (from x in MainConfig.Article.AsEnumerable()
                    where x[kp.Key].ToString() == ""
                    select x).ToList().ForEach(x => x[kp.Key] = r.Next(600000, 699999).ToString() + ".");
                    MessageBox.Show( "Spalte: " + kp.Key + " wurde angereichert. \r\n");
                    !!!
                    */
                    #endregion
                }
                else
                {
                    if (expectedType != curType)
                    {
                        errMsg += "Spalte: " + kp.Key + " entspricht nicht dem Datentyp: " + expectedType.ToString() + "\r\n";
                    }
                }
            }
            if (errMsg != "")
            {
                MessageBox.Show(errMsg + "Bitte formatieren Sie die entspechende Datenquelle: " + AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString()));
                return -1;
            }
            return 0;
        }
        public static string CheckCurrentDbPath(bool changePath)
        {
            System.Windows.Forms.FolderBrowserDialog objDialog = new FolderBrowserDialog();
            objDialog.Description = "Bitte wählen Sie das Verzeichnis der Access Datenbank aus (FOEU.mdb oder FOCH.mdb)";
            string dbPath = AppSettingHandler.GetAppSetting(SettingNames.DbPath.ToString());
            if (changePath == true) { dbPath = string.Empty; }
            if (CheckIfDbExists(dbPath))
            {
                MainConfig.mainAppConfiguration.MyFolder = dbPath;
            }
            else
            {
                if (objDialog.ShowDialog() == DialogResult.OK)
                {
                    AppSettingHandler.SetAppSetting(SettingNames.DbPath.ToString(), objDialog.SelectedPath);
                    MainConfig.mainAppConfiguration.MyFolder = objDialog.SelectedPath;
                }
            }
            SendCurrentDbPath(GetCurrentDbPath(MainConfig.mainAppConfiguration.MyFolder));
            SetDatabaseName(MainConfig.conf.Waehrung);

            return MainConfig.mainAppConfiguration.MyFolder;
        }
        public static string GetCurrentDbPath(string folder)
        {
            return "Db-Path: " + folder;
        }
        public static void SetDatabaseName(Currency waehrung)
        {
            string currentDbFile = AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString());
            switch (waehrung)
            {
                case Currency.Eur:
                    AppSettingHandler.SetAppSetting(SettingNames.dbWaehrung.ToString(), MainConfig.mainAppConfiguration.DbFileEur);
                    break;
                case Currency.Chf:
                    AppSettingHandler.SetAppSetting(SettingNames.dbWaehrung.ToString(), MainConfig.mainAppConfiguration.DbFileChf);
                    break;
                case Currency.ChfEur:
                    AppSettingHandler.SetAppSetting(SettingNames.dbWaehrung.ToString(), "");
                    frmChooseDatabase frm = new frmChooseDatabase();
                    frm.Show();
                    break;
            }
            currentDbFile = AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString());
            if (!string.IsNullOrEmpty(currentDbFile))
            {
                LoadDatabase(currentDbFile, MainConfig.mainAppConfiguration.SortimentTableName);
            }
        }
        public static bool LoadDatabase(string dbName, string tbName)
        {
            if (dbName.ToLower() == "excel")
            {
                DataTable temp = ExcelHandler.CheckExcelFileSource(MainConfig.mainAppConfiguration.SortimentTableName, "Bitte wählen Sie die neue Datenquelle", "");
                if (temp.Rows.Count > 0)
                {
                    MainConfig.mainAppConfiguration.Article = temp;
                    AppSettingHandler.SetAppSetting(SettingNames.dbWaehrung.ToString(), "excel");
                    SendCurrentDbPath(GetCurrentDbPath(MainConfig.mainAppConfiguration.MyFolder));
                    PrintHandler.SetDbAgeWarning();
                };
            }
            else
            {
                DataTable import = AccessHandler.GetArticleTableFromAccess(dbName, tbName);
                if (import.Columns.Contains("VK30")) { import.Columns["VK30"].ColumnName = "VK"; }
                if (import.Columns.Contains("VK01")) { import.Columns["VK01"].ColumnName = "VK"; }
                MainConfig.mainAppConfiguration.Article = import;
                if (MainConfig.mainAppConfiguration.Article.Rows.Count > 0)
                {
                    PrintHandler.SetDbAgeWarning();
                }
            }
            return SettingsAfterSuccessfulDbImport();
        }
        public static bool CheckAgeOfDatabase()
        {
            bool result = false;
            DateTime writeTime = File.GetCreationTime(Path.Combine(MainConfig.mainAppConfiguration.MyFolder, AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString())));
            DateTime actualTime = DateTime.Now;
            string warningText = "ACHTUNG! Die Datenbank ist veraltet."
                + "Bitte Updaten."
                + Environment.NewLine + "mailto: sdpmue@hotmail.com"
                + Environment.NewLine + "creationtime: " + writeTime;
            int dbMonth = writeTime.Month;
            int dbYear = writeTime.Year;
            int actYear = actualTime.Year;
            int actMonth = actualTime.Month;
            int ageOfdatabase = (actMonth - dbMonth) + (12 * (actYear - dbYear));
            if (!((ageOfdatabase >= 0) && (ageOfdatabase <= 3)))
            {
                if (AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString()).ToLower() != "excel")
                {
                    SendWarningText(warningText);
                    result = true;
                }
            }
            return result;
        }

        static void AddOttoArticleNumberToDataTable()
        {
            DataTable ottoItemnumbers = TryToGetOttoItemnumberFromExcel(MainConfig.conf.ottoExcelSettings.SheetName);
            if (ottoItemnumbers.Rows.Count > 0)
            {
                if (ottoItemnumbers.Columns.Contains(MainConfig.conf.ottoExcelSettings.OttoEanNumberColumnname))
                {
                    ToggleCursor(true);
                    foreach (DataRow dr in ottoItemnumbers.Rows)
                    {
                        (from x in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                         where (x["EAN-Nr1W"].ToString() == dr[MainConfig.conf.ottoExcelSettings.OttoEanNumberColumnname].ToString())
                         select x).ToList().ForEach(x => x["ottoartikel"] = dr[MainConfig.conf.ottoExcelSettings.OttoItemnumberColumnname]);
                    }

                    var test = from y in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                               where y["ottoartikel"].ToString() != ""
                               select y;
                }
                ToggleCursor(false);
            }
        }
        static DataTable TryToGetOttoItemnumberFromExcel(string sheetName)
        {
            DataTable ottoNumbers = ExcelHandler.CheckExcelFileSource(
                sheetName
                ,
                "Bitte wählen Sie die Datei Mit den Otto-Artikelnummern"
                ,
                MainConfig.conf.ottoExcelSettings.OttoExcelFileName
                );
            return ottoNumbers;
        }
        static bool CheckIfDbExists(string dbPath)
        {
            string dbPathEUR = Path.Combine(dbPath, MainConfig.mainAppConfiguration.DbFileEur);
            string dbPathCHF = Path.Combine(dbPath, MainConfig.mainAppConfiguration.DbFileChf);
            bool result = false;
            if (File.Exists(dbPathEUR)) { return true; }
            else if (File.Exists(dbPathCHF)) { return true; }
            return result;
        }
        static bool SettingsAfterSuccessfulDbImport()
        {
            string articleCount = MainConfig.mainAppConfiguration.Article.Rows.Count.ToString();
            if (MainConfig.mainAppConfiguration.Article.Rows.Count == 0)
            {
                SendRecordCount(GetCurrentDbPath(articleCount + " Datensätze verfügbar"));
                return false;
            }
            else
            {
                SendRecordCount(GetCurrentDbPath(articleCount + " Datensätze verfügbar"));
                SendMainFormTitle(MainConfig.GetFormTitle(MainConfig.mainAppConfiguration.AssemblyVersion));
            }
            try
            {
                string port = AppSettingHandler.GetAppSetting(SettingNames.ComPort.ToString()).ToUpper();
                if (MainConfig.mainAppConfiguration.Init && port.Contains("COM"))
                {
                    ComInterfaceHandler.ComOpenPort(port);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }
    }
}
