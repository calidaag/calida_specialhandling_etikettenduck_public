﻿
namespace SpecialHandlingEtikettenDruckV3
{
    class CalidaArticle
    {
        public string Scanned { get; set; }
        public string EanNr1 { get; set; }
        public string EanNr2 { get; set; }
        public string ArtikelBeschreibung { get; set; }
        public string FarbBezeichnung { get; set; }
        public double ArtNummer { get; set; }
        public double Color { get; set; }
        public string Size { get; set; }
        public string Kollektion { get; set; }
        public double DemodPreis { get; set; }
        public double ZweitWahlPreis { get; set; }
        public double VerkaufsPreis { get; set; }
        public string Waehrung { get; set; }
        public string Deklaration { get; set; }
        public string DisplayMember { get; set; }
        public string OttoVersandDeEigeneArtikelnummer { get; set; }
    }
}
