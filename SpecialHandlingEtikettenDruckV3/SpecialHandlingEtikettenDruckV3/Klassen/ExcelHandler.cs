﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace SpecialHandlingEtikettenDruckV3
{
    class ExcelHandler
    {
        public static DataTable CheckExcelFileSource(string sheetName, string title, string filter)
        {
            string excelFolder = "";
            DataTable result = new DataTable();
            System.Windows.Forms.OpenFileDialog objDialog = new OpenFileDialog();
            objDialog.Title = title;
            string filename = "";
            if (String.IsNullOrEmpty(filter)) { objDialog.Filter = "Excel |*.xlsx"; }
            else { objDialog.Filter = "Excel |"+filter; 
            }
            objDialog.FilterIndex = 1;
            objDialog.Multiselect = true;
            if (objDialog.ShowDialog() == DialogResult.OK)
            {
                filename = objDialog.FileName;
                try { excelFolder = Path.GetDirectoryName(filename); }
                catch (Exception e)
                {
                    var x = e.Message;
                }
            }
            else
            {
                filename = string.Empty;
            }
            result = GetArticleTableFromExcelSheet(sheetName, filename);
            return result;
        }
        static DataTable GetArticleTableFromExcelSheet(string sheetName, string filename)
        {
            DataTable dtImport = new DataTable();
            if (!String.IsNullOrEmpty(filename))
            {
                using (System.Data.OleDb.OleDbConnection myConnection = new System.Data.OleDb.OleDbConnection(
                            "Provider=Microsoft.ACE.OLEDB.12.0; " +
                             "data source='" + filename + "';" +
                                "Extended Properties=\"Excel 12.0;HDR=YES;IMEX=0\" "))
                {
                    using (System.Data.OleDb.OleDbDataAdapter myImportCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [" + sheetName + "$]", myConnection))
                        try { myImportCommand.Fill(dtImport); }
                        catch (Exception e) { MessageBox.Show("Die ausgewählte Excel-Datei enthält möglicheweise kein Tabellenblatt mit dem Namen " + sheetName + "\r\n" + e.Message); }
                }
            }
            return dtImport;
        }
    }

    class ExcelConfigurationsSetForOttoVersand
    {
        public string SheetName { get; set; }
        public string OttoExcelFileName { get; set; }
        public string OttoItemnumberColumnname { get; set; }
        public string OttoEanNumberColumnname { get; set; }
    }
}
