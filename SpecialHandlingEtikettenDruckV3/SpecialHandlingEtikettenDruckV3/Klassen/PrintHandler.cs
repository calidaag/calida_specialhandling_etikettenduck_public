﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SpecialHandlingEtikettenDruckV3
{
    class PrintHandler
    {
        public delegate void SetWarningOnOff(bool state);
        public static event SetWarningOnOff SetWarning;
        public delegate void SetComInterface();
        public static event SetComInterface SetCom;
        public delegate void SetTcpIpInterface();
        public static event SetTcpIpInterface SetTcpIp;

        public static void SetDbAgeWarning()
        {
            SetWarning(DatabaseManagement.CheckAgeOfDatabase());
        }
        public static void PrintLabel(string labelContent)
        {
            SetDbAgeWarning();
            if (MainConfig.mainAppConfiguration.PrintMode == PrinterInterface.Com)
            {
                try
                {
                    System.IO.File.WriteAllText(MainConfig.mainAppConfiguration.MyFolder + @"\CurrentLabel.txt", ";" + DateTime.Now + "\r\n" + labelContent, Encoding.GetEncoding(1252));
                }
                catch (Exception e) { MessageBox.Show(e.Message); }
                if (MainConfig.mainAppConfiguration.ComPort.IsOpen)
                {
                    try
                    {
                        MainConfig.mainAppConfiguration.ComPort.Write(labelContent);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
                else { MessageBox.Show("der COM-Port ist geschlossen"); }
            }
            else if (MainConfig.mainAppConfiguration.PrintMode == PrinterInterface.TcpIp)
            {
                string path = Path.Combine(MainConfig.mainAppConfiguration.MyFolder, "CurrentLabel.txt");
                File.WriteAllText(path, labelContent);
                TcpIpSocket printerSocket = new TcpIpSocket();
                printerSocket.NetworkPrint(MainConfig.mainAppConfiguration.IpAddress, path);
            }
        }
        static PrinterInterface getPrinterMode() {
            PrinterInterface printerInterface;
            Enum.TryParse(AppSettingHandler.GetAppSetting(SettingNames.PrintMode.ToString()), out printerInterface);
            return  printerInterface;
        }
        public static bool PrintModeChange(PrinterInterface newMode)
        {
            bool result = false;
            switch (newMode)
            {
                case PrinterInterface.Com:
                    MainConfig.mainAppConfiguration.PrintMode = PrinterInterface.Com;
                    AppSettingHandler.SetAppSetting(SettingNames.PrintMode.ToString(), PrinterInterface.Com.ToString());
                    ComInterfaceHandler.ComOpenPort(AppSettingHandler.GetAppSetting(SettingNames.ComPort.ToString()));
                    break;

                case PrinterInterface.TcpIp:
                    MainConfig.mainAppConfiguration.PrintMode = PrinterInterface.TcpIp;
                    AppSettingHandler.SetAppSetting(SettingNames.PrintMode.ToString(), PrinterInterface.TcpIp.ToString());
                    break;

                case PrinterInterface.Unknown:
                    MainConfig.mainAppConfiguration.PrintMode = getPrinterMode();
                    break;
            }
            if (MainConfig.mainAppConfiguration.PrintMode == PrinterInterface.Com)  { SetCom(); }
            if (MainConfig.mainAppConfiguration.PrintMode == PrinterInterface.TcpIp) { SetTcpIp(); }
            return result;
        }
    }
}
