﻿using System;
using System.Collections.Generic;

namespace SpecialHandlingEtikettenDruckV3
{
    class LabelStyleHandler
    {
        public delegate void LabelInfoChange(string msg);
        public static event LabelInfoChange StorenameDescriptionChanged;
        public static event LabelInfoChange StoreTitleChanged;
        public static string GetNewLabel(CalidaArticle calArt, LabelMode layout)
        {
            string label = "no Data";
            if (calArt == null) { layout = LabelMode.UserDefined; }
            switch (layout)
            {
                case LabelMode.Zalando: label = CreateZalandoLabel(calArt);
                    break;
                case LabelMode.Retouren: label = CreateRetourenLabel(calArt);
                    break;
                case LabelMode.ManuellesLager: label = CreateRetourenLabel(calArt);
                    break;
                case LabelMode.Zweitwahl: label = Create2WahlLabel(calArt);
                    break;
                case LabelMode.Ottoversand: label = CreateOttoVersandDeLabel(calArt);
                    break;
                case LabelMode.CalidaStore: label = CreateCalidaStoreLabel(new KeyValuePair<string, CalidaArticle>(MainConfig.conf.StoreLocation, calArt));
                    break;
                case LabelMode.UserDefined:
                    break;
            }
            return label;
        }

        static string CreateCalidaStoreLabel(KeyValuePair<string, CalidaArticle> calidaStoreNameArticle)
        {
            string titel = "Calida-Store";
            CalidaArticle calArt = calidaStoreNameArticle.Value;
            string storeName = calidaStoreNameArticle.Key;
            StorenameDescriptionChanged(storeName);
            StoreTitleChanged(titel);
            string label = "";

            if (String.IsNullOrEmpty(calArt.Deklaration)) { calArt.Deklaration = "unverb.Richtpreis"; }

            label = ";Druckerinitialisierung" + Environment.NewLine
                                            + ";Job-Start-Kommando" + Environment.NewLine
                                            + "J" + Environment.NewLine
                                            + ";Definition der Etikettegrösse" + Environment.NewLine
                                            + "Sl1;0,0,38,40,44" + Environment.NewLine
                                            + ";Spendemodus" + Environment.NewLine
                                            + "P" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + GetEarmark()
                                            + "T2.0,4.0,0,3,2.71,q91;" + titel + Environment.NewLine
                                            + "T2.0,7.0,0,3,2.71,q91;" + storeName + Environment.NewLine
                // + "T2.0,7.0,0,3,2.71,q91;" + CheckUmlauts(calArt.ArtikelBeschreibung) + Environment.NewLine
                // + "T2.0,7.5,0,3,3.3,b;" + CheckUmlauts(String.Format("{0:0.00}", calArt.VerkaufsPreis)) + " " + calArt.Waehrung + Environment.NewLine
                // + "T2.0,10.0,0,3,2.71,q91;" + CheckUmlauts(calArt.FarbBezeichnung) + Environment.NewLine
                                            + "T27.0,5.0,0,-5,x1,y1;" + calArt.ArtNummer + "[J:r15]" + Environment.NewLine
                                            + "T27.0,9.0,0,-5,x1,y1;" + calArt.Color + "[J:r15]" + Environment.NewLine
                                            + "G2.0,11.5,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + "T2.0,19.0,0,5,7.76,q82;" + calArt.Size + Environment.NewLine
                                            + "T2.0,22.5,0,3,2.71,q91;" + Environment.NewLine//+ DateTime.Now + Environment.NewLine
                //+ ";Wenn Auszeichnungsart <> E ist (E=Ohne Auszeichnung)" + Environment.NewLine
                                            + "T17.0,19.0,0,5,7.76,q82;" + String.Format("{0:0.00}", calArt.VerkaufsPreis) + "[J:r25]" + Environment.NewLine
                                            + "T32.0,22.5,0,5,2.34,q100;" + calArt.Waehrung + "[J:r10]" + Environment.NewLine
                //+ ";Wenn Auszeichnungsart R ist (R=Empfohlener Richtpreis)" + Environment.NewLine
                                            + "T17.0,22.5,0,3,2.34,q89;" + calArt.Deklaration.ToString() + "[J:r19]" + Environment.NewLine
                                            + "G2.0,24.0,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + ";Barcode EAN-13" + Environment.NewLine
                                            + "B2.0,25.0,0,f,8.0,0.25;" + calArt.Scanned + Environment.NewLine
                                            + "T2.0,35.5,0,3,2.71,q91;" + calArt.Scanned + Environment.NewLine
                //   + "T27.0,27.0,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,30.5,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,34.0,0,-5,x1,y1;712[J:r15]" + Environment.NewLine
                                            + ";Wenn Sonderbearbeitung erforderlich ist" + Environment.NewLine
                                            + ";T32.0,29.0,0,3,6.0;*[J:r5]" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + ";Job-Ende mit Anzahl Etiketten" + Environment.NewLine
                                            + "A " + MainConfig.mainAppConfiguration.ActAmount.ToString() + Environment.NewLine;
            return label;
        }
        static string CreateRetourenLabel(CalidaArticle calArt)
        {
            string demodPreis = "###.##";
            string verkaufsPreis = "###.##";
            if ((calArt.DemodPreis > 0) && (calArt.VerkaufsPreis > 0))
            {
                demodPreis = String.Format("{0:0.00}", calArt.DemodPreis);
                verkaufsPreis = String.Format("{0:0.00}", calArt.VerkaufsPreis);
            }
            string titel = "ursprüngl. Verkaufspreis";
            StoreTitleChanged(titel);
            string label = "";
            if (String.IsNullOrEmpty(calArt.Deklaration)) { calArt.Deklaration = "unverb.Richtpreis"; }

            label = ";Druckerinitialisierung" + Environment.NewLine
                                            + ";Job-Start-Kommando" + Environment.NewLine
                                            + "J" + Environment.NewLine
                                            + ";Definition der Etikettegrösse" + Environment.NewLine
                                            + "Sl1;0,0,38,40,44" + Environment.NewLine
                                            + ";Spendemodus" + Environment.NewLine
                                            + "P" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + GetEarmark()
                                            + "T2.0,4.0,0,3,2.71,q91;" + titel + Environment.NewLine
                // + "T2.0,7.0,0,3,2.71,q91;" + CheckUmlauts(calArt.ArtikelBeschreibung) + Environment.NewLine
                                            + "T2.0,7.5,0,3,3.3,b;" + String.Format("{0:0.00}", verkaufsPreis) + " " + calArt.Waehrung + Environment.NewLine
                // + "T2.0,10.0,0,3,2.71,q91;" + CheckUmlauts(calArt.FarbBezeichnung) + Environment.NewLine
                                            + "T27.0,5.0,0,-5,x1,y1;" + calArt.ArtNummer + "[J:r15]" + Environment.NewLine
                                            + "T27.0,9.0,0,-5,x1,y1;" + calArt.Color + "[J:r15]" + Environment.NewLine
                                            + "G2.0,11.5,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + "T2.0,19.0,0,5,7.76,q82;" + calArt.Size + Environment.NewLine
                                            + "T2.0,22.5,0,3,2.71,q91;" + Environment.NewLine//+ DateTime.Now + Environment.NewLine
                //+ ";Wenn Auszeichnungsart <> E ist (E=Ohne Auszeichnung)" + Environment.NewLine
                //+ "T17.0,19.0,0,5,7.76,q82;" + String.Format("{0:0.00}", calArt.DemodPreis) + "[J:r25]" + Environment.NewLine
                                            + "T17.0,19.0,0,5,7.76,q82;" + demodPreis + "[J:r25]" + Environment.NewLine
                                            + "T32.0,22.5,0,5,2.34,q100;" + calArt.Waehrung + "[J:r10]" + Environment.NewLine
                //+ ";Wenn Auszeichnungsart R ist (R=Empfohlener Richtpreis)" + Environment.NewLine
                                            + "T17.0,22.5,0,3,2.34,q89;" + calArt.Deklaration.ToString() + "[J:r19]" + Environment.NewLine
                                            + "G2.0,24.0,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + ";Barcode EAN-13" + Environment.NewLine
                                            + "B2.0,25.0,0,f,8.0,0.25;" + calArt.Scanned + Environment.NewLine
                                            + "T2.0,35.5,0,3,2.71,q91;" + calArt.Scanned + Environment.NewLine
                //   + "T27.0,27.0,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,30.5,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,34.0,0,-5,x1,y1;712[J:r15]" + Environment.NewLine
                                            + ";Wenn Sonderbearbeitung erforderlich ist" + Environment.NewLine
                                            + ";T32.0,29.0,0,3,6.0;*[J:r5]" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + ";Job-Ende mit Anzahl Etiketten" + Environment.NewLine
                                            + "A " + MainConfig.mainAppConfiguration.ActAmount.ToString() + Environment.NewLine;
            return label;
        }
        static string CreateZalandoLabel(CalidaArticle calArt)
        {
            string titel = "zalando";
            StoreTitleChanged(titel);
            string label = "";

            label = ";Druckerinitialisierung" + Environment.NewLine
                                            + ";Job-Start-Kommando" + Environment.NewLine
                                            + "J" + Environment.NewLine
                                            + ";Definition der Etikettegrösse" + Environment.NewLine
                                            + "Sl1;0,0,38,40,44" + Environment.NewLine
                                            + ";Spendemodus" + Environment.NewLine
                                            + "P" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + GetEarmark()
                                            + "T2.0,4.0,0,3,2.71,q91;" + titel + Environment.NewLine
                                            + "T2.0,7.0,0,3,2.71,q91;" + calArt.ArtikelBeschreibung + Environment.NewLine
                                            + "T2.0,10.0,0,3,2.71,q91;" + calArt.FarbBezeichnung + Environment.NewLine
                                            + "T27.0,5.0,0,-5,x1,y1;" + calArt.ArtNummer + "[J:r15]" + Environment.NewLine
                                            + "T27.0,9.0,0,-5,x1,y1;" + calArt.Color + "[J:r15]" + Environment.NewLine
                                            + "G2.0,11.5,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + "T2.0,19.0,0,5,7.76,q82;" + calArt.Size + Environment.NewLine
                                            + "T2.0,22.5,0,3,2.71,q91;" + Environment.NewLine//+ DateTime.Now + Environment.NewLine
                //+ ";Wenn Auszeichnungsart <> E ist (E=Ohne Auszeichnung)" + Environment.NewLine
                //+ "T17.0,19.0,0,5,7.76,q82;19.90[J:r25]" + Environment.NewLine
                //+ "T32.0,22.5,0,5,2.34,q100;CHF[J:r10]" + Environment.NewLine
                //+ ";Wenn Auszeichnungsart R ist (R=Empfohlener Richtpreis)" + Environment.NewLine
                //+ "T17.0,22.5,0,3,2.34,q89;unverb. Richtpreis[J:r19]" + Environment.NewLine
                                            + "G2.0,24.0,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + ";Barcode EAN-13" + Environment.NewLine
                                            + "B2.0,25.0,0,f,8.0,0.25;" + calArt.Scanned + Environment.NewLine
                                            + "T2.0,35.5,0,3,2.71,q91;" + calArt.Scanned + Environment.NewLine
                //   + "T27.0,27.0,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,30.5,0,-5,x1,y1;3[J:r15]" + Environment.NewLine
                // + "T27.0,34.0,0,-5,x1,y1;712[J:r15]" + Environment.NewLine
                                            + ";Wenn Sonderbearbeitung erforderlich ist" + Environment.NewLine
                                            + ";T32.0,29.0,0,3,6.0;*[J:r5]" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + ";Job-Ende mit Anzahl Etiketten" + Environment.NewLine
                                            + "A " + MainConfig.mainAppConfiguration.ActAmount.ToString() + Environment.NewLine;
            return label;
        }
        static string Create2WahlLabel(CalidaArticle calArt)
        {
            string zweitWahlPreis = "###.##";
            string verkaufsPreis = "###.##";
            if ((calArt.ZweitWahlPreis > 0) && (calArt.VerkaufsPreis > 0))
            {
                zweitWahlPreis = String.Format("{0:0.00}", calArt.ZweitWahlPreis);
                verkaufsPreis = String.Format("{0:0.00}", calArt.VerkaufsPreis);
            }
            string titel = "ursprüngl. Verkaufspreis";
            StoreTitleChanged(titel);
            string label = "";
            if (String.IsNullOrEmpty(calArt.Deklaration)) { calArt.Deklaration = "unverb.Richtpreis"; }

            label = ";Druckerinitialisierung" + Environment.NewLine
                                            + ";Job-Start-Kommando" + Environment.NewLine
                                            + "J" + Environment.NewLine
                                            + ";Definition der Etikettegrösse" + Environment.NewLine
                                            + "Sl1;0,0,38,40,44" + Environment.NewLine
                                            + ";Spendemodus" + Environment.NewLine
                                            + "P" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + GetEarmark()
                                            + "T2.0,4.0,0,3,2.71,q91;" + titel + Environment.NewLine
                                            + "T2.0,7.5,0,3,3.3,b;" + String.Format("{0:0.00}", verkaufsPreis) + " " + calArt.Waehrung + Environment.NewLine
                                            + "T27.0,5.0,0,-5,x1,y1;" + calArt.ArtNummer + "[J:r15]" + Environment.NewLine
                                            + "T27.0,9.0,0,-5,x1,y1;" + calArt.Color + "[J:r15]" + Environment.NewLine
                                            + "G2.0,11.5,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + "T2.0,19.0,0,5,7.76,q82;" + calArt.Size + Environment.NewLine
                                            + "T2.0,22.5,0,3,2.71,q91;" + Environment.NewLine
                                            + "T17.0,19.0,0,5,7.76,q82;" + zweitWahlPreis + "[J:r25]" + Environment.NewLine
                                            + "T32.0,22.5,0,5,2.34,q100;" + calArt.Waehrung + "[J:r10]" + Environment.NewLine
                                            + "T17.0,22.5,0,3,2.34,q89;" + calArt.Deklaration.ToString() + "[J:r19]" + Environment.NewLine
                                            + "G2.0,24.0,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + ";Barcode EAN-13" + Environment.NewLine
                                            + "B2.0,25.0,0,f,8.0,0.25;" + calArt.EanNr2 + Environment.NewLine //sollte hier nicht EAN2 gedruckt werden?
                                            + "T2.0,35.5,0,3,2.71,q91;" + calArt.EanNr2 + Environment.NewLine //sollte hier nicht EAN2 gedruckt werden?
                                            + ";Wenn Sonderbearbeitung erforderlich ist" + Environment.NewLine
                                            + ";T32.0,29.0,0,3,6.0;*[J:r5]" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + ";Job-Ende mit Anzahl Etiketten" + Environment.NewLine
                                            + "A " + MainConfig.mainAppConfiguration.ActAmount.ToString() + Environment.NewLine;
            return label;
        }
        static string CreateOttoVersandDeLabel(CalidaArticle calArt)
        {
            string label = "";
            label = ";Druckerinitialisierung" + Environment.NewLine
                                            + ";Job-Start-Kommando" + Environment.NewLine
                                            + "J" + Environment.NewLine
                                            + ";Definition der Etikettegrösse" + Environment.NewLine
                                            + "Sl1;0,0,38,40,44" + Environment.NewLine
                                            + ";Spendemodus" + Environment.NewLine
                                            + "P" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + GetEarmark()
                                            + "T7.0,7.0,0,3,8.0,b;" + calArt.OttoVersandDeEigeneArtikelnummer + Environment.NewLine
                                            //+ "T2.0,7.5,0,3,3.3,b;" + String.Format("{0:0.00}", verkaufsPreis) + " " + calArt.Waehrung + Environment.NewLine
                                            //+ "T27.0,5.0,0,-5,x1,y1;" + calArt.ArtNummer + "[J:r15]" + Environment.NewLine
                                            //+ "T27.0,9.0,0,-5,x1,y1;" + calArt.Color + "[J:r15]" + Environment.NewLine
                                            //+ "G2.0,11.5,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + "T20,17.0,0,5,8.0,q82;" + calArt.Size + Environment.NewLine
                                            //+ "T2.0,22.5,0,3,2.71,q91;" + Environment.NewLine
                                            //+ "T17.0,19.0,0,5,7.76,q82;" + zweitWahlPreis + "[J:r25]" + Environment.NewLine
                                            //+ "T32.0,22.5,0,5,2.34,q100;" + calArt.Waehrung + "[J:r10]" + Environment.NewLine
                                            //+ "T17.0,22.5,0,3,2.34,q89;" + calArt.Deklaration.ToString() + "[J:r19]" + Environment.NewLine
                                            //+ "G2.0,24.0,0;L:40.0,0.5[S:100]" + Environment.NewLine
                                            + ";Barcode EAN-13" + Environment.NewLine
                                            + "B2.0,20.0,0,f,13.0,0.33;" + calArt.EanNr1 + Environment.NewLine //sollte hier nicht EAN2 gedruckt werden?
                                            //+ "B2.0,20.0,0,f,13.0,0.25;" + calArt.EanNr1 + Environment.NewLine //sollte hier nicht EAN2 gedruckt werden?
                                            + "T2.0,35.5,0,3,2.71,q91;" + calArt.EanNr1 + Environment.NewLine //sollte hier nicht EAN2 gedruckt werden?
                                            // + ";Wenn Sonderbearbeitung erforderlich ist" + Environment.NewLine
                                            // + ";T32.0,29.0,0,3,6.0;*[J:r5]" + Environment.NewLine
                                            + ";------------------------------------------------" + Environment.NewLine
                                            + ";Job-Ende mit Anzahl Etiketten" + Environment.NewLine
                                            + "A " + MainConfig.mainAppConfiguration.ActAmount.ToString() + Environment.NewLine;
            return label;
        }
        static string GetEarmark() {
            return "T42.0,36.0,0,3,2.71,b;." + Environment.NewLine;
        }
    }
}
