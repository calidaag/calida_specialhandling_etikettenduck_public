﻿using System;
using System.Text;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    class ComInterfaceHandler
    {
        public delegate void InterfaceChange(string msg);
        public static event InterfaceChange InterfaceChanged;

        public static void ComOpenPort(string portName)
        {
            string comPortNameBefore = MainConfig.mainAppConfiguration.ComPort.PortName;
            if (MainConfig.mainAppConfiguration.PrintMode == PrinterInterface.Com && CheckComPortValidity(portName))
            {
                try
                {
                    MainConfig.mainAppConfiguration.ComPort.Close();
                    MainConfig.mainAppConfiguration.ComPort.PortName = portName;
                    MainConfig.mainAppConfiguration.ComPort.Encoding = Encoding.GetEncoding(1252);
                    MainConfig.mainAppConfiguration.ComPort.Open();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    if (!MainConfig.mainAppConfiguration.ComPort.IsOpen)
                    {
                        try { MainConfig.mainAppConfiguration.ComPort.Open(); }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                InterfaceChanged("Druckerschnittstelle: " + MainConfig.mainAppConfiguration.ComPort.PortName);
            }
        }
        static bool CheckComPortValidity(string comport)
        {
            bool result = false;
            int portNumber = -1;
            string numbers = "";
            if (comport.ToLower().Contains("com"))
            {
                numbers = comport.Remove(0, 3);
                if (Int32.TryParse(numbers, out portNumber))
                {
                    if (portNumber > -1)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }       
    }
}
