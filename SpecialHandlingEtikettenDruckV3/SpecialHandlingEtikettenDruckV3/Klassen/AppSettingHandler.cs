﻿using System.Configuration;

namespace SpecialHandlingEtikettenDruckV3
{
    class AppSettingHandler
    {
        public static void SetAppSetting(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( System.Reflection.Assembly.GetExecutingAssembly().Location);
            if (config.AppSettings.Settings[key] != null)
            {
                config.AppSettings.Settings.Remove(key);
            }
            config.AppSettings.Settings.Add(key, value);
            config.Save(ConfigurationSaveMode.Modified);
        }
        public static string GetAppSetting(string key)
        {
            string result = "";
            Configuration config = ConfigurationManager.OpenExeConfiguration( System.Reflection.Assembly.GetExecutingAssembly().Location);
            if (config.AppSettings.Settings[key] != null)
            {
                try
                {
                    result = config.AppSettings.Settings[key].Value;
                }
                catch { }
            }
            else
            {
                SetAppSetting(key, "null");
            }
            return result;
        }
    }

    enum SettingNames
    {
        DbPath
        ,
        dbWaehrung
        ,
        ComPort
        ,
        LabelStyle
        ,
        IpAddress
        , 
        PrintMode
    };
}
