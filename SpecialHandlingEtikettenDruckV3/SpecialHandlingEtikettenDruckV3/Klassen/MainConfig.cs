﻿using System;
using System.Data;
using System.IO.Ports;
using System.Collections.Generic;

namespace SpecialHandlingEtikettenDruckV3
{
    class MainConfig
    {
        //********** SOFTWARE-SWITCH (Assembly parametrieren) ********************************************************
        public static ModeConfigurationSet altConf = AppModeSwitch(LabelMode.Retouren,""); 
        //************************************************************************************************************
        
        public static ApplicationConfigurationSet mainAppConfiguration = SetApplicationSettings();
        public static ModeConfigurationSet conf = new ModeConfigurationSet();

        public static string GetFormTitle(string version)
        {
            string appName = "Etiketten Druck V";
            string signatur = "@2014 by Daniel Mueller";
            //string titel = appName + version + " " + signatur + " " + "(" + AppSettingHandler.GetAppSetting(SettingNames.LabelStyle.ToString()).ToUpper() + ")";
            string titel = appName + version + " " + signatur + " " + "(" + MainConfig.conf.LabelStyle.ToString().ToUpper() + ")";
           
            switch (MainConfig.conf.LabelStyle)
            {
                case LabelMode.CalidaStore: titel = appName + version + " " + signatur + " " + "(" + MainConfig.conf.LabelStyle.ToString().ToUpper() + " " + MainConfig.conf.StoreLocation + ")";
                    break;
            }
            return titel + " " + AppSettingHandler.GetAppSetting(SettingNames.dbWaehrung.ToString()); ;
        }
        public static void TryToChangeAppMode(string[] parameter)
        {
            LabelMode mode = altConf.LabelStyle;
            string storeName = "";
            if (parameter.Length >= 1)
            {
                if (Enum.TryParse(parameter[0], true, out mode))
                {
                    if (parameter.Length == 2)
                    {
                        storeName = parameter[1];
                    }
                }
                conf = AppModeSwitch(mode, storeName);
            }
            else
            {
                conf = altConf;
            }
        }
        public static List<KeyValuePair<string, object>> GetMandatoryColumsForSearchEngine()
        {
            List<KeyValuePair<string, object>> mandatoryColums = new List<KeyValuePair<string, object>>();
            mandatoryColums.Add(new KeyValuePair<string, object>("EAN-Nr1W", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("EAN-Nr2W", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("Artikel", (double)0));
            mandatoryColums.Add(new KeyValuePair<string, object>("FarbCode", (double)0));
            mandatoryColums.Add(new KeyValuePair<string, object>("Grösse", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("A-Bezeichnung D", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("F-Bezeichnung D", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("AusTabelle", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("VKDemod", (double)0));
            mandatoryColums.Add(new KeyValuePair<string, object>("Vk", (double)0));
            mandatoryColums.Add(new KeyValuePair<string, object>("Währung", (string)""));
            mandatoryColums.Add(new KeyValuePair<string, object>("Vk2W", (double)0));
            mandatoryColums.Add(new KeyValuePair<string, object>("OttoArtikel", (string)""));
            return mandatoryColums;
        }
        
        static ApplicationConfigurationSet SetApplicationSettings()
        {
            return new ApplicationConfigurationSet
            {
                DbFileEur = @"FOEU.mdb"
                ,
                DbFileChf = @"FOCH.mdb"
                ,
                SortimentTableName = "sortiment"
                ,
                MyFolder = ""
                ,
                Article = new DataTable()
                ,
                AssemblyVersion = ""
                ,
                Init = false
                ,
                ComPort = new SerialPort("com1")
                ,
                PrintMode = PrinterInterface.Unknown
                ,
                IpAddress = ""
                ,
                LastPrint = new CalidaArticle()
                ,
                TboDefaultEan = "Geben sie hier einen 13 Stelligen EAN-Code ein"
                ,
                ActAmount = 1
                ,
                FrmTitle = ""
            };
        }
        static ModeConfigurationSet AppModeRetourenBeelitz()
        {
            /***************************************************************************************************************
             *  5.5.2015 DEMODIERTER-PREIS ! Retouren-Etiketten wurden für das Retouren-Lager in Beelitz (Nur EURO!) entwickelt. 
             *  Es wird der ursprüngliche Verkaufspreis und der demodierte Preis gedruckt.   
             ***************************************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.Retouren
                ,
                Waehrung = Currency.Eur
                ,
                StoreLocation = ""
                ,
                AllowUserDefinedLabel = false
            };
            return conf;
        }
        static ModeConfigurationSet AppModeManuellesLager()
        {
            /***************************************************************************************************************
             *    5.5.2015 Gleiche Etikette wie "retouren" jedoch CHF oder EURO wählbar!!!
             *    Entwickelt für manuelles Lager in Sursee
             ***************************************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.ManuellesLager //LabelMode.Retouren
                ,
                Waehrung = Currency.ChfEur
                ,
                StoreLocation = ""
                ,
                AllowUserDefinedLabel = false
            };
            return conf;
        }
        static ModeConfigurationSet AppModeZweitwahl()
        {
            /***************************************************************************************************
             * 5.4.2016 ZWEITWAHL-PREIS! Es soll EURO und CHF unterstützt werden (waehrung auf "chfeur" setzen).  
             * Es wird der ursprüngliche Verkaufspreis und der 2.Wahl-Preis gedruckt.
             ***************************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.Zweitwahl
                ,
                Waehrung = Currency.ChfEur
                ,
                StoreLocation = ""
                ,
                AllowUserDefinedLabel = false
            };
            return conf;
        }
        static ModeConfigurationSet AppModeZalando()
        {
            /**********************************************************************************************
             * 5.5.2015 Zalando wird im Special Handling gedruckt. 
             * Der Preis wird nicht gedruckt (Daher Währung nicht relevant). 
             * Der Etikett-Titel muss "zalando" sein.
             **********************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.Zalando
                ,
                Waehrung = Currency.Eur
                ,
                StoreLocation = ""
                ,
                AllowUserDefinedLabel = false
            };
            return conf;
        }
        static ModeConfigurationSet AppModeCalidaStore(string storeName)
        {
            /**********************************************************************************************
            * 5.5.2015 Dieser Store wurde von Calida übernommen. 
            * Nur der Verkaufspreis wird gedruckt. 
            * Die CHF Datenbank muss ausgewählt werden!!!
            ***********************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.CalidaStore
                ,
                Waehrung = Currency.Chf
                ,
                StoreLocation = storeName
                ,
                AllowUserDefinedLabel = false
            };
            return conf;
        }
        static ModeConfigurationSet AppModeOttoVersandDe()
        {
            /**********************************************************************************************
            * 9.9.2016 Die Etiketten für Otto-Versand DE sollen im Special-Handling gedruckt und geklebt werden
            * Es werden keine Preise gedruckt. (Daher ist die Währug nicht relevant)
            * Falls Otto-Eigene Artikelnummer nicht verfügbar soll sie von Hand eingegeben werden können. Deshalb UserDefined erlauben
            ***********************************************************************************************/
            ModeConfigurationSet conf = new ModeConfigurationSet
            {
                LabelStyle = LabelMode.Ottoversand
                ,
                Waehrung = Currency.Chf
                ,
                StoreLocation = ""
                ,
                AllowUserDefinedLabel = true
                ,
            };
            conf.ottoExcelSettings = new ExcelConfigurationsSetForOttoVersand
            {
                OttoEanNumberColumnname = "EAN"
                ,
                OttoItemnumberColumnname = "ZZKDART1"
                ,
                SheetName = "Z313_Otto"
                ,
                OttoExcelFileName = "Z313_Otto.xlsx"
            };
            return conf;
        }
        static ModeConfigurationSet AppModeSwitch(LabelMode mode, string storeName)
        {
            switch (mode)
            {
                case LabelMode.CalidaStore: return AppModeCalidaStore(storeName);

                case LabelMode.Ottoversand: return AppModeOttoVersandDe();

                case LabelMode.ManuellesLager: return AppModeManuellesLager();

                case LabelMode.Retouren: return AppModeRetourenBeelitz();

                case LabelMode.Zalando: return AppModeZalando();

                case LabelMode.Zweitwahl: return AppModeZweitwahl();

                default: return new ModeConfigurationSet();
            }
        }
    }

    public enum Currency { Chf, Eur, ChfEur };
    enum LabelMode { Retouren, ManuellesLager, Zweitwahl, Zalando, CalidaStore, Ottoversand, UserDefined };
    enum PrinterInterface { Com, TcpIp, Unknown };

    class ModeConfigurationSet
    {
        public Currency Waehrung { get; set; }
        public LabelMode LabelStyle { get; set; }
        public string StoreLocation { get; set; }
        public bool AllowUserDefinedLabel { get; set; }
        public ExcelConfigurationsSetForOttoVersand ottoExcelSettings = new ExcelConfigurationsSetForOttoVersand();
    }
    class ApplicationConfigurationSet
    {
        public string DbFileEur { get; set; }
        public string DbFileChf { get; set; }
        public string SortimentTableName { get; set; }
        public string MyFolder { get; set; }
        public string AssemblyVersion { get; set; }
        public DataTable Article { get; set; }
        public bool Init { get; set; }
        public SerialPort ComPort { get; set; }
        public PrinterInterface PrintMode { get; set; }
        public string IpAddress { get; set; }
        public CalidaArticle LastPrint { get; set; }
        public string TboDefaultEan { get; set; }
        public int ActAmount { get; set; }
        public string FrmTitle { get; set; }
    }
}
