﻿using System.Collections.Generic;
using System.Linq;
using System.Data;
using System;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    class SearchEngine
    {
        public delegate void UpdateSelectionArea(object data);
        public static event UpdateSelectionArea SendData;
        public delegate void SetDisplayMember(string member);
        public static event SetDisplayMember SetMember;

        public static List<CalidaArticle> SearchArticleInAssortment(string eanNummer)
        {
            List<CalidaArticle> res = new List<CalidaArticle>();
            if (eanNummer.StartsWith(","))
            {
                eanNummer = GetEanFromParameter(eanNummer);
            }
            try
            {
                if (DatabaseManagement.CheckIfDataSourceIsValid() == 0)
                {
                    res = (from dataRecord in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                           where dataRecord.Field<string>("EAN-Nr1W") == eanNummer || dataRecord.Field<string>("EAN-Nr2W") == eanNummer
                           select new CalidaArticle
                           {
                               EanNr1 = dataRecord.Field<string>("EAN-Nr1W")
                               ,
                               EanNr2 = dataRecord.Field<string>("EAN-Nr2W")
                               ,
                               ArtNummer = dataRecord.Field<double>("Artikel")
                               ,
                               Color = dataRecord.Field<double>("FarbCode")
                               ,
                               Size = dataRecord.Field<string>("Grösse")
                               ,
                               ArtikelBeschreibung = dataRecord.Field<string>("A-Bezeichnung D")
                               ,
                               FarbBezeichnung = dataRecord.Field<string>("F-Bezeichnung D")
                               ,
                               Kollektion = dataRecord.Field<string>("AusTabelle")
                               ,
                               Scanned = eanNummer
                               ,
                               DemodPreis = dataRecord.Field<double>("VKDemod")
                               ,
                               VerkaufsPreis = dataRecord.Field<double>("VK")
                               ,
                               Waehrung = dataRecord.Field<string>("Währung")
                               ,
                               ZweitWahlPreis = dataRecord.Field<double>("VK2W")
                               ,
                               OttoVersandDeEigeneArtikelnummer = dataRecord.Field<string>("OttoArtikel")
                           }).ToList();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return res;
        }
        public static double GetEanThroughSelection(string parameter)
        {
            double isNumber = 0;
            string[] param = parameter.Split(',');
            string par1 = "";
            string par2 = "";
            string par3 = "";
            string par4 = "";
            param = (from s in param
                     select s.TrimStart('0')).ToArray();

            List<object> objList = new List<object>();

            if (Double.TryParse(param[1], out isNumber)) { par1 = param[1]; }
            else { if (param[1].Length > 3) { par4 = param[1]; } }

            if ((param.Count() > 1) && (!String.IsNullOrEmpty(param[1])))
            {
                par1 = param[1];
                if (!Double.TryParse(param[1], out isNumber)) { if (param[1].Length > 3) { par1 = ""; par4 = param[1]; } }
            }
            if ((param.Count() > 2) && (!String.IsNullOrEmpty(param[2])))
            {
                par2 = param[2];
                if (!Double.TryParse(param[2], out isNumber)) { if (param[2].Length > 3) { par2 = ""; par4 = param[2]; } }
            }
            if ((param.Count() > 3) && (!String.IsNullOrEmpty(param[2])))
            {
                par3 = param[3];
                if (!Double.TryParse(param[3], out isNumber)) { if (param[3].Length > 3) { par3 = ""; par4 = param[3]; } }
            }
            Dictionary<string, object> articleColorSize = GetEanFromArticleColorSize(parameter, false);
            if (par1.Length > 1 || par2.Length > 0 || par3.Length > 0 || par4.Length > 0)
            {
                if (DatabaseManagement.CheckIfDataSourceIsValid() == 0)
                {
                    try
                    {
                        var res = (from afg in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                                   where
                                                                       afg.Field<double>("Artikel").ToString().Contains(par1.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par2.ToString()) && afg.Field<string>("Grösse").Contains(par3.ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                                                    || afg.Field<double>("Artikel").ToString().Contains(par2.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par1.ToString()) && afg.Field<string>("Grösse").Contains(par3.ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                                                    || afg.Field<double>("Artikel").ToString().Contains(par3.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par2.ToString()) && afg.Field<string>("Grösse").Contains(par1.ToString().ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                                                    || afg.Field<double>("Artikel").ToString().Contains(par2.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par3.ToString()) && afg.Field<string>("Grösse").Contains(par1.ToString().ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                                                    || afg.Field<double>("Artikel").ToString().Contains(par1.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par3.ToString()) && afg.Field<string>("Grösse").Contains(par2.ToString().ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                                                    || afg.Field<double>("Artikel").ToString().Contains(par3.ToString()) && afg.Field<double>("FarbCode").ToString().Contains(par1.ToString()) && afg.Field<string>("Grösse").Contains(par2.ToString().ToUpper()) && (afg.Field<string>("A-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()) || afg.Field<string>("F-Bezeichnung D").ToLower().Contains(par4.ToString().ToLower()))
                                   select new CalidaArticle
                                   {
                                       EanNr1 = afg.Field<string>("EAN-Nr1W")
                                       ,
                                       ArtNummer = afg.Field<double>("Artikel")
                                       ,
                                       ArtikelBeschreibung = afg.Field<string>("A-Bezeichnung D")
                                       ,
                                       FarbBezeichnung = afg.Field<string>("F-Bezeichnung D")
                                       ,
                                       Color = afg.Field<double>("FarbCode")
                                       ,
                                       Size = afg.Field<string>("Grösse")
                                       ,
                                       DisplayMember = afg.Field<double>("Artikel").ToString()
                                                       + ", " + afg.Field<double>("FarbCode").ToString()
                                                       + ", " + afg.Field<string>("Grösse")
                                                       + "\t\t" + afg.Field<string>("A-Bezeichnung D")
                                                       + ", " + afg.Field<string>("F-Bezeichnung D")

                                   }).ToList<object>();

                        objList = res;
                    }
                    catch (Exception e) { MessageBox.Show(e.Message); }
                }
            }
            SendData(objList);
            SetMember("DisplayMember");
            var result = objList.Count();
            return result;
        }
        static string GetEanFromParameter(string parameter)
        {
            string ean = "0000000000000";
            Dictionary<string, object> articleColorSize = GetEanFromArticleColorSize(parameter, false);
            var res = from afg in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                      where afg.Field<double>("Artikel").Equals(articleColorSize["article"])
                      && afg.Field<double>("FarbCode").Equals(articleColorSize["color"])
                      && afg.Field<string>("Grösse") == articleColorSize["size"].ToString().ToUpper()
                      select afg.Field<string>("EAN-Nr1W");
            if (res.Count() == 1)
            {
                ean = res.First().ToString();
            }
            else
            {
                articleColorSize = GetEanFromArticleColorSize(parameter, true);
                if (DatabaseManagement.CheckIfDataSourceIsValid() == 0)
                {
                    var res2 = from afg in MainConfig.mainAppConfiguration.Article.AsEnumerable().AsParallel()
                               where afg.Field<double>("Artikel").Equals(articleColorSize["article"])
                               && afg.Field<double>("FarbCode").Equals(articleColorSize["color"])
                               && afg.Field<string>("Grösse") == articleColorSize["size"].ToString().ToUpper()
                               select afg.Field<string>("EAN-Nr1W");
                    if (res2.Count() == 1)
                    {
                        ean = res.First().ToString();
                    }
                }
            }
            return ean;
        }
        static Dictionary<string, object> GetEanFromArticleColorSize(string parameter, bool exchangeValue)
        {
            Dictionary<string, object> articleColorSize = new Dictionary<string, object>();

            string[] param = parameter.Split(',');
            double articleNummer = 0;
            double color = 0;
            string size = "";

            articleColorSize.Add("article", articleNummer);
            articleColorSize.Add("color", color);
            articleColorSize.Add("size", size);

            param = (from s in param
                     orderby s.Length descending
                     select s.TrimStart('0')).ToArray();

            if (Double.TryParse(param.First(), out articleNummer)) { articleColorSize["article"] = articleNummer; }

            if (param.Count() == 4)
            {
                if (exchangeValue == false)
                {
                    if (Double.TryParse(param[1], out color)) { articleColorSize["color"] = color; }
                    size = param[2];
                    articleColorSize["size"] = size;
                }
                if (exchangeValue == true)
                {
                    if (Double.TryParse(param[2], out color)) { articleColorSize["color"] = color; }
                    size = param[1];
                    articleColorSize["size"] = size;
                }
            }
            return articleColorSize;
        }
    }
}
