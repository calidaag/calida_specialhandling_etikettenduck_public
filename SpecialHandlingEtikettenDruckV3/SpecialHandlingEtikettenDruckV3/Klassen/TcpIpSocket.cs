﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    class TcpIpSocket
    {
        public bool NetworkPrint(string ipAdress, string filePath)
        {
            bool result = false;
            try
            {
                Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSocket.NoDelay = true;
                IPAddress ip = IPAddress.Parse(ipAdress);
                IPEndPoint ipep = new IPEndPoint(ip, 9100);
                clientSocket.Connect(ipep);
                byte[] fileBytes = File.ReadAllBytes(filePath);
                clientSocket.Send(fileBytes);
                clientSocket.Close();
                result = true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return result;
        }
    }
}
