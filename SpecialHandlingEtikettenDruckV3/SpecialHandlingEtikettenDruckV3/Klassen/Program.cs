﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpecialHandlingEtikettenDruckV3
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Only for Testing
            // args = new string[] { "retouren" };
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainConfig.TryToChangeAppMode(args);
            Application.Run(new frmMainStartup());
        }
    }
}
