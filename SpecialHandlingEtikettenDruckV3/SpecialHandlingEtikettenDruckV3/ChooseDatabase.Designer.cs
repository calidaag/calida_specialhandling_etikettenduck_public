﻿namespace SpecialHandlingEtikettenDruckV3
{
    partial class frmChooseDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseDatabase));
            this.gbxChooseDb = new System.Windows.Forms.GroupBox();
            this.radBtnChf = new System.Windows.Forms.RadioButton();
            this.radBtnEuro = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.gbxChooseDb.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxChooseDb
            // 
            this.gbxChooseDb.Controls.Add(this.radBtnChf);
            this.gbxChooseDb.Controls.Add(this.radBtnEuro);
            this.gbxChooseDb.Location = new System.Drawing.Point(12, 12);
            this.gbxChooseDb.Name = "gbxChooseDb";
            this.gbxChooseDb.Size = new System.Drawing.Size(253, 145);
            this.gbxChooseDb.TabIndex = 0;
            this.gbxChooseDb.TabStop = false;
            this.gbxChooseDb.Text = "Datenbank wählen";
            // 
            // radBtnChf
            // 
            this.radBtnChf.AutoSize = true;
            this.radBtnChf.Location = new System.Drawing.Point(78, 90);
            this.radBtnChf.Name = "radBtnChf";
            this.radBtnChf.Size = new System.Drawing.Size(89, 17);
            this.radBtnChf.TabIndex = 1;
            this.radBtnChf.Text = "Preise in CHF";
            this.radBtnChf.UseVisualStyleBackColor = true;
            // 
            // radBtnEuro
            // 
            this.radBtnEuro.AutoSize = true;
            this.radBtnEuro.Checked = true;
            this.radBtnEuro.Location = new System.Drawing.Point(78, 49);
            this.radBtnEuro.Name = "radBtnEuro";
            this.radBtnEuro.Size = new System.Drawing.Size(99, 17);
            this.radBtnEuro.TabIndex = 0;
            this.radBtnEuro.TabStop = true;
            this.radBtnEuro.Text = "Preise in EURO";
            this.radBtnEuro.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(102, 163);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(68, 29);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmChooseDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 204);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.gbxChooseDb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmChooseDatabase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChooseDatabase";
            this.gbxChooseDb.ResumeLayout(false);
            this.gbxChooseDb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxChooseDb;
        private System.Windows.Forms.RadioButton radBtnChf;
        private System.Windows.Forms.RadioButton radBtnEuro;
        private System.Windows.Forms.Button btnOk;
    }
}